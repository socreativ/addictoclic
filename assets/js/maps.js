class Maps {
  constructor(params) {
    this.mapElement = params.mapElement || document.getElementById("map");
    this.dataUrl = params.dataUrl;
    this.keyAPI = params.keyAPI;

    this.map = new google.maps.Map(this.mapElement);
    this.centers = [];
    this.markers = [];
    this.search = {};
    this.cluster = null;
    this.template = document.getElementById(params.template);
    this.infoW;
    this.markerImg =
      "https://addictoclic.com/wp-content/themes/theme-by-socreativ/assets/img/marker/markerclusterer/m4.png";

    this.initSearch();
    this.getData(this.dataUrl);
    this.setupModal();
  }

  initList() {
    let onloadCount = 0;

    this.centers.sort((a, b) =>
      a.NOM.toLowerCase() > b.NOM.toLowerCase() ? 1 : -1
    );
    this.centers.forEach((e, i) => {
      let c = this.template.cloneNode(true);

      let isSearched = true;
      if (Object.keys(this.search).length !== 0) {
        for (const [key, value] of Object.entries(this.search)) {
          if (!Array.isArray(value)) {
            if (isSearched)
              isSearched = e[key].startsWith(value) ? true : false;
          } else {
            if (isSearched)
              isSearched = e[key].includes(value.join(", ")) ? true : false;
          }
        }
      }

      c.classList.add("location");
      if (!isSearched) {
        c.style.opacity = 0;
        c.style.padding = 0;
        c.style.height = 0;
        c.style.pointerEvents = "none";
        if (window.innerWidth < 576) {
          c.animate(
            {
              minWidth: 0,
              fontSize: 0,
              padding: 0,
            },
            { easing: "ease", fill: "forwards", duration: 350 }
          );
        }
        // end add by loic
      } else {
        onloadCount++;
        c.classList.add("visible");
      }
      const t = document.querySelector("#theme_dir").innerHTML;
      c.setAttribute("id", "location-" + e.ID);
      c.querySelector(".loc-title").innerHTML = e.NOM;
      c.querySelector(".loc-city").innerHTML =
        e.ADRESSE + ", <strong>" + e.VILLE + "</strong>";
      c.querySelector(".loc-dep").innerHTML = e.CP + ", " + e.DEP;
      c.querySelector(
        ".loc-tel"
      ).innerHTML = `<img src="${t}/assets/img/phone-2.svg" /> ${e.TEL1}`;
      c.querySelector(".loc-mail").setAttribute("href", "mailto:" + e.MAIL);
      c.querySelector(
        ".loc-mail"
      ).innerHTML = `<img src="${t}/assets/img/ic_email_24px.svg" /> ${e.MAIL}`;
      c.addEventListener("click", function () {
        let m = document.querySelector(".modal");
        if (e.ACR != "")
          m.querySelector(".modal-title").innerHTML =
            e.NOM + " (" + e.ACR + ")";
        if (e.ACR == "") m.querySelector(".modal-title").innerHTML = e.NOM;
        m.querySelector(".loc-type").querySelector("span").innerText =
          e.STRUCTURE;
        m.querySelector(".loc-act").querySelector("span").innerText =
          e.ACTIVITE;
        m.querySelector(".loc-aide").querySelector("span").innerText = e.AIDE;
        m.querySelector(".loc-prod").querySelector("span").innerText =
          e.PRODUITS;
        m.querySelector(".loc-team").querySelector("span").innerText = e.EQUIPE;
        m.querySelector(".loc-info").querySelector("span").innerText = e.INFO;
        m.querySelector(".loc-public").querySelector("span").innerText =
          e.PUBLIC;
        m
          .querySelector(".loc-web")
          .querySelector(
            "span"
          ).innerHTML = `<a target="_blank" href="https://${e.WEB}">${e.WEB}</a>`;

        m.querySelector(".loc-adresse").querySelector("span").innerHTML =
          e.ADRESSE +
          ", " +
          e.CPL +
          " <strong>" +
          e.VILLE +
          "</strong><br>" +
          e.CP +
          ", " +
          e.DEP;
        m.querySelector(".loc-tel1").querySelector("span").innerText = e.TEL1;
        m.querySelector(".loc-tel2").querySelector("span").innerText = e.TEL2;
        if (e.TEL2 == "") {
          m.querySelector(".loc-tel2").setAttribute("hidden", "true");
        } else {
          m.querySelector(".loc-tel2").removeAttribute("hidden");
        }
        m
          .querySelector(".loc-mail")
          .querySelector(
            "span"
          ).innerHTML = `<a href="mailto:${e.MAIL}">${e.MAIL}</a>`;
        m.querySelector(".loc-fax").querySelector("span").innerText = e.FAX;
        m.querySelector(".loc-hor").querySelector("span").innerText =
          e.HORAIRES;
        m.querySelector(".loc-rdv").querySelector("span").innerText = e.RDV;

        let url =
          window.location.protocol +
          "//" +
          window.location.host +
          "/formulaire-etablissement/?edit&ID=" +
          e.ID;
        m.querySelector("a.modify").setAttribute("href", url);

        m.querySelector(".print-btn").addEventListener("click", function () {
          let content = `<h1>${e.NOM} (${e.ACR})</h1>`;
          content += m.querySelector(".modal-body").innerHTML;
          content += `<style>.modal-body{position:relative;padding:15px;}.row{display:flex;flex-wrap:wrap;}.col-md-6{flex:0 0 50%;max-width:50%}h4{font-size:20px;color:#E2735C;}</style>`;
          var a = window.open("", "", "height=500, width=500");
          a.document.write("<html><body>");
          a.document.write(content);
          a.document.write("</body></html>");
          a.document.close();
          a.print();
        });

        m.style.display = "block";
        setTimeout(() => {
          m.classList.add("show");
        }, 350);
      });

      this.template.parentNode.insertBefore(c, this.template);
    });
    if (onloadCount == 0) {
      if (this.search.CP != "") {
        document.querySelector(".empty-center .rm-CP").style.display = "block";
        document.querySelector(".empty-center .reset").style.display = "none";
      } else {
        document.querySelector(".empty-center .rm-CP").style.display = "none";
        document.querySelector(".empty-center .reset").style.display = "block";
      }
      document.querySelector(".empty-center").style.opacity = 1;
    } else {
      document.querySelector(".empty-center").style.opacity = 0;
    }
    document.querySelector("#list-count").querySelector("span").innerText =
      onloadCount;
    this.template.remove();
  }

  setupModal() {
    let m = document.querySelector(".modal");
    let close = m.querySelectorAll(".exit");
    close.forEach((c) => {
      c.addEventListener("click", () => {
        m.classList.remove("show");
        setTimeout(() => {
          m.style.display = "none";
        }, 350);
      });
    });
  }

  updateCP(cp = '33', zoomValue = 11) {
    this.search.CP = cp;
    this.clearMap();
    this.setMarker();
    this.filterList();
    this.map.setZoom(zoomValue)
  }

  newSearch(r, preciseLocation = { lat: 44.393149, lng: -0.579180 },  zoomGauge = 7) {
    this.clearMap();
    this.search = r;
    this.setMarker();
    this.filterList();
    setTimeout(() => {
      this.map.setZoom(zoomGauge);
      this.map.setCenter(preciseLocation);
    }, 100)
  }

  filterList() {
    const all = Array.from(document.querySelectorAll(".location"));
    const id = this.markers.map((m) => "location-" + m.obj.ID);
    const keep = all.filter((e) => id.includes(e.id));
    const remove = all.filter((e) => !id.includes(e.id));

    remove.forEach((e) => {
      e.style.opacity = "0";
      e.style.pointerEvents = "none";

      setTimeout(() => {
        e.classList.remove("visible");
        if (window.innerWidth < 576) {
          e.animate(
            {
              minWidth: 0,
              fontSize: 0,
              padding: 0,
            },
            { easing: "ease", fill: "forwards", duration: 350 }
          );
        } else {
          e.animate(
            {
              height: 0,
              padding: 0,
            },
            { easing: "ease", fill: "forwards", duration: 350 }
          );
        }
      }, 350);
    });

    keep.forEach((e, i) => {
      e.classList.add("visible");
      let color = i % 2 == 0 ? "#F0F0F0" : "#FFFFFF";
      e.style.backgroundColor = color;
      if (window.innerWidth < 576) {
        e.animate(
          {
            minWidth: "60vw",
            fontSize: "1em",
            padding: "1em",
          },
          { easing: "ease", fill: "forwards", duration: 350 }
        );
      } else {
        e.animate(
          {
            height: "auto",
            padding: "1em",
          },
          { easing: "ease", fill: "forwards", duration: 350 }
        );
      }
      setTimeout(() => {
        e.style.opacity = "1";
        e.style.pointerEvents = "all";
      }, 350);
    });

    if (keep.length == 0) {
      if (this.search.CP != "") {
        document.querySelector(".empty-center .rm-CP").style.display = "block";
        document.querySelector(".empty-center .reset").style.display = "none";
      } else {
        document.querySelector(".empty-center .rm-CP").style.display = "none";
        document.querySelector(".empty-center .reset").style.display = "block";
      }
      document.querySelector(".empty-center").style.opacity = 1;
    } else {
      document.querySelector(".empty-center").style.opacity = 0;
    }

    document.querySelector("#list-count").querySelector("span").innerText =
      keep.length;
  }

  // attach each marker to the given map => if map is null markers disapear
  setMapOnAll(m) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(m);
    }
  }

  // clear all markers, cluster stored and clear map
  clearMap() {
    this.setMapOnAll(null);
    this.cluster.clearMarkers();
    this.markers = [];
  }

  // Create markers for each center stored depending search parameters => if search params are null create a maker for each center
  setMarker() {
    // case search params are null
    if (Object.keys(this.search).length === 0) {
      this.markers = this.centers.map((center, i) => {
        return new google.maps.Marker({
          icon: this.markerImg,
          position: {
            lat: parseFloat(center.COORD.split(",")[0]),
            lng: parseFloat(center.COORD.split(",")[1]),
          },
          obj: center,
        });
      });
    } else {
      // case search params are set
      let process = this.centers;
      for (const [key, value] of Object.entries(this.search)) {
        process = process.filter((center) => {
          if (!Array.isArray(value)) {
            return center[key].startsWith(value);
          } else {
            let formated = value.join(", ");
            return center[key].includes(formated);
          }
        });
      }
      this.markers = process.map((center, i) => {
        return new google.maps.Marker({
          icon: this.markerImg,
          position: {
            lat: parseFloat(center.COORD.split(",")[0]),
            lng: parseFloat(center.COORD.split(",")[1]),
          },
          obj: center,
        });
      });
    }

    // makers array is created so init them with map, eventlistener and other stuff
    this.markers.forEach((marker) => {
      marker.setMap(this.map);
      this.oms.addMarker(marker);

      marker.addListener("spider_click", () => {
        this.map.getZoom() < 12
          ? this.map.setZoom(12)
          : this.map.setZoom(this.map.getZoom());
        this.map.setCenter(marker.getPosition());
        setTimeout(() => {
          this.infoW.setContent(
            `<h3 class="marker-title">${marker.obj.NOM}</h3><p>Type : ${marker.obj.ACTIVITE}</p><p>RDV : ${marker.obj.RDV}</p><p>Public : ${marker.obj.PUBLIC}</p><p>Produits : ${marker.obj.PRODUITS}</p>`
          );
          this.infoW.open(this.map, marker);
        }, 150);
      });

      marker.addListener("mouseover", () => {
        const target = document.getElementById("location-" + marker.obj.ID);
        target.scrollIntoView({ behavior: "smooth", block: "center" });
        let blinking = false;
        if (!blinking) {
          setTimeout(() => {
            blinking = true;
            target.style.transform = "scale(1.1)";
            setTimeout(() => {
              target.style.transform = "scale(1)";
              setTimeout(() => {
                blinking = false;
              }, 350);
            }, 350);
          }, 350);
        }
      });
    });

    // create the cluster with markers array
    this.cluster = new MarkerClusterer(this.map, this.markers, {
      maxZoom: 12,
      imagePath:
        "https://addictoclic.com/wp-content/themes/theme-by-socreativ/assets/img/marker/markerclusterer/m",
    });
  }

  // if center has no coordinate set in csv file, get coordinate from google api with address given
  setGeoCoord() {
    this.centers.map(async (center, i) => {
      if (center["COORD"] === "") {
        const address = `${center.ADRESSE}+${center.VILLE}+${center.CP}`;
        const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${this.keyAPI}`;
        const req = new XMLHttpRequest();
        req.open("GET", url, false);
        req.onreadystatechange = function (e) {
          if (this.readyState == 4 && this.status == 200) {
            const res = JSON.parse(this.responseText);
            center.COORD = `${res.results[0].geometry.location.lat},${res.results[0].geometry.location.lng}`;
          }
        };
        req.send();
      }
    });
    this.setMarker(this.centers);
  }

  // Get param from url
  initSearch() {
    if (window.location.search != "") {
      const paramaters = window.location.search.substr(1).split("&");
      paramaters.forEach((el) => {
        let filter = el.split("=");
        console.log(filter[1]);
        filter[1] = filter[1].replaceAll('%20', ' ');
        console.log(filter[1]);
        if (filter[1].includes(",")) {
          this.search[filter[0]] = filter[1].split(",");
        } else if (filter[1].includes("%2C")) {
          this.search[filter[0]] = filter[1].split("%2C");
        } else {
          this.search[filter[0]] = filter[0] === 'PRODUITS' || filter[0] === 'PUBLIC' ? filter[1].split(' ') : filter[1];
        }
      });
    }
  }

  initOverlapingMarker(map) {
    return new OverlappingMarkerSpiderfier(map, {
      markersWontMove: true,
      markersWontHide: true,
      basicFormatEvents: true,
    });
  }

  // Init map object
  initMap() {
    this.map = new google.maps.Map(document.getElementById("map"), {
      zoom: 7,
      center: { lat: 44.837789, lng: -0.579180 },
      mapId: "1e7ba97c74c9d1b4",
      streetViewControl: false,
      mapTypeControl: false,
      mapTypeId: "roadmap",
    });
    this.infoW = new google.maps.InfoWindow({
      content: "",
    });
    this.oms = this.initOverlapingMarker(this.map);
    this.setGeoCoord();
  }

  // Create request to csv file
  getData(url) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Content-type", "text");
    const that = this;
    xhttp.send();
    xhttp.onreadystatechange = function (e) {
      if (this.readyState == 4 && this.status == 200) {
        that.centers = that.csvJSON(this.responseText);
        that.initMap();
        that.initList();
      } else if (this.status == 404) {
        console.error("File not found !");
      }
    };
  }

  // Transform raw data from csv in javascript array
  csvJSON(csv) {
    const lines = csv.split("\r\n");
    lines.pop();
    const headers = lines[0].split(";");
    return lines.map((l, index) => {
      const cl = l.split(";");
      const obj = {};
      for (let i = 0; i < headers.length; i++) {
        obj[headers[i]] = cl[i];
        obj["ID"] = index - 1;
      }
      return obj;
    });
  }
}

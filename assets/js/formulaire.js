("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {
        $(".center-form").each(() => {


            let step1 = {}, step2 = {}, from = {};

            Filter.dataUrl = $('.center-form').attr('filters');
            Filter.currentSearch = null;

            // Step 1

            Filter.parent = document.querySelector('.forms.step1');

            var form1 = {
                input: {
                    ACR: new Filter('ACR', 'texte', false, "Ex: NDE", "Acronyme*"),
                    STRUCTURE: new Filter('STRUCTURE', "select", false, "Sélectionnez", "Type*"),
                    ACTIVITE: new Filter('ACTIVITE', "select", false, "Sélectionnez", "Activité*"),
                    AIDE: new Filter('AIDE', "select", false, "Sélectionnez", "Aide*"),
                    PRODUITS: new Filter('PRODUITS', "select", true, "choix multiples", "Produit(s)*"),
                    EQUIPE: new Filter('EQUIPE', "select", true, "choix multiples", "Équipe*"),
                    INFO: new Filter('INFO', "texte", false, "Ex: référent", "Infos Complémentaires"),
                    PUBLIC: new Filter('PUBLIC', "select", true, "choix multiples", "Public*"),
                    PRIX: new Filter('PRIX', "select", false, "Sélectionnez", "Prix*"),
                    RDV: new Filter('RDV', "select", false, "Sélectionnez", "RDV*")
                },
                required: ["NOM", "STRUCTURE", "ACTIVITE", "AIDE", "PRODUITS", "EQUIPE", "PUBLIC", "PRIX", "RDV"]
                //required: ["NOM"]
            }

            setTimeout(() => {
                let i=0;
                for (const key in form1.input) {
                    form1.input[key].setOrder(i);
                    i++;
                    if(form1.required.includes(key)){
                        form1.input[key].element.setAttribute('required', true);
                    }
                }
              }, 500);

              $("#end-step1").click(() => {
                 let valid = true;
                 let err = "";
                 step1["NOM"] = $("#name").val();
                 if(step1["NOM"] == "") valid = false; err += `Le champs "NOM" est vide ou invalide. \n`;

                 $(".forms.step1 .filtres").each(function (){
                    let input = $(this).find('select')[0]
                    if(input == undefined) input = $(this).find('input')[0];
                    step1[$(input).attr('name')] = $(input).val();

                    if(form1.required.includes($(input).attr('name')) && ($(input).val() == "" || $(input).val == [])){
                        valid = false;
                        err += `Le champs "${$(input).attr('name')}" est vide ou invalide. \n`;
                        if($(input)[0].nodeName == "INPUT"){
                            $(input).css({"border" : "solid 2px red"});
                        }
                        else{
                            $($($($(input)[0].parentElement)[0].parentElement)[0].parentElement).css({"border" : "solid 2px red"});
                        }
                    }
                    else{
                        if($(input)[0].nodeName == "INPUT"){
                            $(input).css({"border" : "solid 2px transparent"});
                        }
                        else{
                            $($($($(input)[0].parentElement)[0].parentElement)[0].parentElement).css({"border" : "solid 2px transparent"});
                        }
                    }
                 })

                 if(valid){
                    $("#form1").removeClass('show');
                    $('#progress').html("2/3");
                    $('#step2').css({"opacity":"1"});
                    $("#form2").find('#name-lock').html(step1.NOM);
                    $("#step-back").css({"opacity":"1","pointer-events":"all"})
                    setTimeout(() => {
                        $("#form2").addClass('show');
                    }, 350)
                 }
                 else{
                    console.error(err)
                    const modal = $('.modal-error');
                    let e = err.replaceAll('\n', '<br>')
                    modal.find('#step-error').html(e)
                    modal.show();
                    modal.addClass('show');
                 }

              })


              // step 2

            setTimeout(() => {
                Filter.parent = document.querySelector('.forms.step2');

                var form2 = {
                    input: {
                        ADRESSE: new Filter('ADRESSE', 'texte', false, "Ex: 1 rue de l'Élysée", "Adresse*"),
                        CPL: new Filter('CPL', "texte", false, "Ex: 2e Étage", "Complément d'adresse"),
                        CP: new Filter('CP', "texte", false, "Ex: 33000", "Code Postal*"),
                        VILLE: new Filter('VILLE', "texte", false, "Bordeaux", "Ville*"),
                        DEP: new Filter('DEP', "select", false, "Sélectionnez", "Département*"),
                        COORD: new Filter('COORD', "texte", false, "Ex: 48.8566,2.3522", "Coordonées"),
                        TEL1: new Filter('TEL1', "texte", false, "Ex: 0601020304", "Téléphone 1*"),
                        TEL2: new Filter('TEL2', "texte", false, "Ex: 0601020304", "Téléphone 2"),
                        EMAIL: new Filter('MAIL', "texte", false, "Ex: exemple@gmail.com", "Email*"),
                        FAX: new Filter('FAX', "texte", false, "Ex: 0123456789", "Fax"),
                        WEB: new Filter('WEB', "texte", false, "Ex: exemple.com", "Site Web"),
                        HORAIRES: new Filter('HORAIRES', "texte", false, "Ex: Lundi au Vendredi de 9h à 18h", "Horaires"),
                    },
                    required: ["ADRESSE", "CP", "VILLE", "DEP", "TEL1", "MAIL"]
                    //required: [""]
                }

                setTimeout(() => {
                    let i=0;
                    for (const key in form2.input) {
                        form2.input[key].setOrder(i);
                        i++;
                        if(form2.required.includes(key)){
                            form2.input[key].element.setAttribute('required', true);
                        }
                    }
                  }, 500);

                 // end step 2

                $("#end-step2").click(() => {
                    let valid = true;
                    let err = "";

                    $(".forms.step2 .filtres").each(function (){
                        let input = $(this).find('select')[0]
                        if(input == undefined) input = $(this).find('input')[0];
                        step2[$(input).attr('name')] = $(input).val();

                        if(form2.required.includes($(input).attr('name')) && ($(input).val() == "" || $(input).val == [])){
                            valid = false;
                            err += `Le champs "${$(input).attr('name')}" est vide ou invalide. \n`;
                            if($(input)[0].nodeName == "INPUT"){
                                $(input).css({"border" : "solid 2px red"});
                            }
                            else{
                                $($($($(input)[0].parentElement)[0].parentElement)[0].parentElement).css({"border" : "solid 2px red"});
                            }
                        }
                        else{
                            if($(input)[0].nodeName == "INPUT"){
                                $(input).css({"border" : "solid 2px transparent"});
                            }
                            else{
                                $($($($(input)[0].parentElement)[0].parentElement)[0].parentElement).css({"border" : "solid 2px transparent"});
                            }
                        }
                    });

                    if(valid){
                        $("#form2").removeClass('show');
                        $('#progress').html("3/3");
                        $('#step3').css({"opacity":"1"});
                        $("#step-back").attr("target", "2");
                        $("#validate").css({"opacity":"1","pointer-events":"all"});
                        $("#v-name").html(step1.NOM);

                        $("#v-type").find("span").html(step1.STRUCTURE);
                        $("#v-act").find("span").html(step1.ACTIVITE);
                        $("#v-aide").find("span").html(step1.AIDE);
                        $("#v-prod").find("span").html(step1.PRODUITS.join(", "));
                        $("#v-ekip").find("span").html(step1.EQUIPE.join(", "));
                        if(step1.INFO != "") $("#v-info").find("span").html(step1.INFO); else $("#v-info").hide();
                        $("#v-pub").find("span").html(step1.PUBLIC.join(", "));

                        $("#v-addr").find("span").html(`${step2.ADRESSE} <br> ${step2.VILLE} ${step2.CP}, ${step2.DEP}`);
                        if(step2.COORD != "") $("#v-gps").find("span").html(step2.COORD); else $("#v-gps").hide();
                        $("#v-tel").find("span").html(step2.TEL1);
                        if(step2.TEL2 != "") $("#v-tel2").find("span").html(step2.TEL2); else $("#v-tel2").hide();
                        if(step2.FAX) $("#v-fax").find("span").html(step2.FAX); else $("#v-fax").hide();
                        $("#v-mail").find("span").html(step2.MAIL);
                        if(step2.WEB != "") $("#v-web").find("span").html(step2.WEB); else $("#v-web").hide();
                        $("#v-hor").find("span").html(step2.HORAIRES);


                        setTimeout(() => {
                            $("#valid-form").addClass('show');
                        }, 350)





                    }else{
                        console.error(err)
                        const modal = $('.modal-error');
                        let e = err.replaceAll('\n', '<br>')
                        modal.find('#step-error').html(e)
                        modal.show();
                        modal.addClass('show');
                    }
                });


            }, 350);

            const adminUrl = $(".toDelete.admin-url").attr('admin-url');
            $(".toDelete.admin-url").remove();

            // validate and sendmail
            $('a#send').click(() => {
                let valid = true;
                let err = "";

                $('.modal-validate input[type="text"]').each(function () {
                    if($(this).val() == ""){
                        valid = false;
                        err += `Le champs ${$(this).attr('placeholder')} est vide ou invalide. \n`;
                    }
                    else{
                        from[$(this).attr('name')] = $(this).val();
                    }
                });

                $('.modal-validate input[type="checkbox"]').each(function() {
                    if($(this).prop("checked") == false){
                        valid = false;
                        err += `Merci de cocher la case : "Je certifie sur l’honneur que les informations fournis correspondent à la réalité et etre en droit de les soumettre à COREADD \n`
                    }
                })

                if(valid){



                    let cUrl = new URL(window.location);
                    let param = cUrl.searchParams.get("edit");  
                    let func = param === null ? "Add" : "Edit";

                    var data = {
                        'action':'sendMail',
                        'func': func,

                        'from_name': from.from_name,
                        'from_mail': from.from_mail,
                        'from_poste': from.from_poste,
                        'from_tel': from.from_tel,

                        'NOM': step1.NOM,
                        "ACR": step1.ACR,
                        "ACTIVITE": step1.ACTIVITE,
                        "AIDE": step1.AIDE,
                        "EQUIPE": step1.EQUIPE,
                        "INFO": step1.INFO,
                        "PRODUITS": step1.PRODUITS,
                        "STRUCTURE": step1.STRUCTURE,
                        "PUBLIC": step1.PUBLIC,
                        "PRIX": step1.PRIX,
                        "RDV": step1.RDV,

                        "ADRESSE": step2.ADRESSE,
                        "COORD": step2.COORD,
                        "CP": step2.CP,
                        "CPL": step2.CPL,
                        "DEP": step2.DEP,
                        "FAX": step2.FAX,
                        "HORAIRES": step2.HORAIRES,
                        "MAIL": step2.MAIL,
                        "TEL1": step2.TEL1,
                        "TEL2": step2.TEL2,
                        "VILLE": step2.VILLE,
                        "WEB": step2.WEB
                    }
                    $.post(adminUrl, data, (response) => {

                        $( '.page-template-template-form .modal-validate .modal-body .row' ).replaceWith( '<div class="row"><div class="col"><h2>Votre message à bien été envoyé !</h2></div></div>' );
                        $( ".page-template-template-form #send" ).remove();
                    });
                }
                else{
                    console.error(err);
                    const modal = $('.modal-error');
                    let e = err.replaceAll('\n', '<br>')
                    modal.find('#step-error').html(e)
                    modal.show();
                    modal.addClass('show');
                }

            });



           // Edit center

            $('.name-list').each(function() {

                let data;
                getData($(this).attr('db-center'));

                function setNameList(center){
                    const currUrl = new URL(window.location.href);
                    let id = currUrl.searchParams.get("ID");
                    if(id != null){
                        center = data[id]
                       setTimeout(() => {
                            var a = $($($($('select[name="STRUCTURE"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<a.length;i++) {
                            if(center.STRUCTURE.includes(a[i].innerHTML)) a[i].click();
                            }
                            var b = $($($($('select[name="ACTIVITE"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<b.length;i++) {
                            if(center.ACTIVITE.includes(b[i].innerHTML)) b[i].click();
                            }
                            var c = $($($($('select[name="AIDE"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<c.length;i++) {
                            if(center.AIDE.includes(c[i].innerHTML)) c[i].click();
                            }
                            var d = $($($($('select[name="PRODUITS"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<d.length;i++) {
                            if(center.PRODUITS.includes(d[i].innerHTML)) setTimeout(() =>  d[i].click(), i*1000);
                            }
                            var e = $($($($('select[name="EQUIPE"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<e.length;i++) {
                            if(center.EQUIPE.includes(e[i].innerHTML)) setTimeout(() =>  e[i].click(), i*1000);
                            }
                            var f = $($($($('select[name="PUBLIC"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<f.length;i++) {
                            if(center.PUBLIC.includes(f[i].innerHTML)) setTimeout(() =>  f[i].click(), i*1000);
                            }
                            g = $($($($('select[name="DEP"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<g.length;i++) {
                            if(center.DEP === g[i].innerHTML) g[i].click();
                            }
                            var h = $($($($('select[name="PRIX"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<h.length;i++) {
                            if(center.PRIX.includes(h[i].innerHTML)) h[i].click();
                            }
                            var j = $($($($('select[name="RDV"]').parent()).parent()).parent().find('ul').find('li'));
                            for (let i=0;i<j.length;i++) {
                            if(center.RDV.includes(j[i].innerHTML)) j[i].click();
                            }
         

                            $('input[name="ACR"]').val(center.ACR);
                            $('input[name="INFO"]').val(center.INFO);
                            $('input[name="ADRESSE"]').val(center.ADRESSE);
                            $('input[name="CPL"]').val(center.CPL);
                            $('input[name="CP"]').val(center.CP);
                            $('input[name="VILLE"]').val(center.VILLE);
                            $('input[name="COORD"]').val(center.COORD);
                            $('input[name="TEL1"]').val(center.TEL1);
                            $('input[name="TEL2"]').val(center.TEL2);
                            $('input[name="MAIL"]').val(center.MAIL);
                            $('input[name="FAX"]').val(center.FAX);
                            $('input[name="WEB"]').val(center.WEB);
                            $('input[name="HORAIRES"]').val(center.HORAIRES);


                            $('.name-locked').html(center.NOM);
                            $('input[name="NOM"').attr('value', center.NOM);
                            $('.name-locked-2').html(center.NOM);
                            $('.name-locked').removeClass('removed');
                            $('.name-list .filtres').remove();
                            $('.forms.step1').show();
                        }, 1000);

                    }
                    else{
                        setTimeout(() => {
                            Filter.parent = document.querySelector('.name-list');
                            let namelist = [];
                            center.forEach(e => {
                                namelist.push(e.NOM);
                            });
                            let list = new Filter('NOM', 'select', false, "Sélectionnez un établissement", null, namelist.sort());
                        }, 1000)
                    }
                }



                $('#valid-editing').click(() => {
                    let target = $("select[name='NOM']").val();
                    if(target == ""){
                        console.error("Aucun établissement sélectionnez");
                    }
                    else{

                        center = data.filter(e => {
                            return e.NOM === target
                        })
                        center = center[0];

                        var a = $($($($('select[name="STRUCTURE"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<a.length;i++) {
                           if(center.STRUCTURE.includes(a[i].innerHTML)) a[i].click();
                        }
                        var b = $($($($('select[name="ACTIVITE"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<b.length;i++) {
                           if(center.ACTIVITE.includes(b[i].innerHTML)) b[i].click();
                        }
                        var c = $($($($('select[name="AIDE"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<c.length;i++) {
                           if(center.AIDE.includes(c[i].innerHTML)) c[i].click();
                        }

                        var d = $($($($('select[name="PRODUITS"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<d.length;i++) {
                           if(center.PRODUITS.includes(d[i].innerHTML)) setTimeout(() =>  d[i].click(), i*1000);
                        }
                        var e = $($($($('select[name="EQUIPE"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<e.length;i++) {
                           if(center.EQUIPE.includes(e[i].innerHTML)) setTimeout(() =>  e[i].click(), i*1000);
                        }
                        var f = $($($($('select[name="PUBLIC"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<f.length;i++) {
                           if(center.PUBLIC.includes(f[i].innerHTML)) setTimeout(() =>  f[i].click(), i*1000);
                        }
                        g = $($($($('select[name="DEP"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<g.length;i++) {
                           if(center.DEP === g[i].innerHTML) g[i].click();
                        }
                        var h = $($($($('select[name="PRIX"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<h.length;i++) {
                        if(center.PRIX.includes(h[i].innerHTML)) h[i].click();
                        }
                        var j = $($($($('select[name="RDV"]').parent()).parent()).parent().find('ul').find('li'));
                        for (let i=0;i<j.length;i++) {
                        if(center.RDV.includes(j[i].innerHTML)) j[i].click();
                        }

                        $('input[name="ACR"]').val(center.ACR);
                        $('input[name="INFO"]').val(center.INFO);
                        $('input[name="ADRESSE"]').val(center.ADRESSE);
                        $('input[name="CPL"]').val(center.CPL);
                        $('input[name="CP"]').val(center.CP);
                        $('input[name="VILLE"]').val(center.VILLE);
                        $('input[name="COORD"]').val(center.COORD);
                        $('input[name="TEL1"]').val(center.TEL1);
                        $('input[name="TEL2"]').val(center.TEL2);
                        $('input[name="MAIL"]').val(center.MAIL);
                        $('input[name="FAX"]').val(center.FAX);
                        $('input[name="WEB"]').val(center.WEB);
                        $('input[name="HORAIRES"]').val(center.HORAIRES);

                        setTimeout(() => {
                            $('#valid-editing').remove();
                            $('input[name="NOM"').attr('value', target)
                            $('.name-locked').html(target);
                            $('.name-locked-2').html(target);
                            $('.name-locked').removeClass('removed');
                            $('.name-list .filtres').remove();
                            $('.forms.step1').show();
                        }, 800)

                    }
                })

                function getData(url){
                    const xhttp = new XMLHttpRequest();
                    xhttp.open('GET', url, true);
                    xhttp.setRequestHeader("Content-type", "text");
                    xhttp.send();
                    xhttp.onreadystatechange = function(e) {
                      if(this.readyState == 4 && this.status == 200){
                        data = csvJSON(this.responseText);
                        setNameList(data);

                      }
                      else if(this.status == 404){
                        console.error('File not found !');
                      }
                    }
                  }

                  // Transform raw data from csv in javascript array
                  function csvJSON(csv){
                    var lines=csv.split("\r\n");
                    var result = [];
                    var headers=lines[0].split(";");
                    for(var i=1;i<lines.length-1;i++){ // -1 to delete last blank caracter which create new line
                        var obj = {};
                        var currentline=lines[i].split(";");
                        for(var j=0;j<headers.length;j++){
                          obj[headers[j]] = currentline[j];
                          obj["ID"] = i-1;
                        }
                        result.push(obj);
                    }
                    return result;
                  }

            });





            // end edit center

            // step back
            $('#step-back').click(function () {
                target = $(this).attr('target');
                switch (target) {
                    case "1":
                        $('#form2').removeClass('show');
                        $('#progress').html("1/3");
                        $('#step2').css({"opacity":"0.4"});
                        $(this).css({"opacity":"0","pointer-events":"none"});
                        setTimeout(() => {
                            $('#form1').addClass('show');
                        }, 350)
                        break;
                    case "2":
                        $(this).attr("target", "1");
                        $('#form2').addClass('show');
                        $('#valid-form').removeClass('show');
                        $('#progress').html("2/3");
                        $('#step3').css({"opacity":"0.4"});
                        $('#validate').css({"opacity":"0","pointer-events":"none"})
                        break;
                    default:
                        console.error('Script fell out in the intersideral space');
                        break;
                }
            })


            $('#validate').each(function (){
                $(this).click(function (){
                    let modal = $('.modal-validate');
                    modal.show();
                    modal.addClass('show');
                });
            });

            $('.modal-validate').each(function (){
                $(this).find('button').each(function (){
                  $(this).click(function (){
                    $('.modal').removeClass('show');
                    setTimeout(() => {
                      $('.modal').hide();
                    }, 350);
                  });
                });
              });


              $('.modal-error').each(function (){
                $(this).find('button').each(function (){
                  $(this).click(function (){
                    $('.modal').removeClass('show');
                    setTimeout(() => {
                      $('.modal').hide();
                    }, 350);
                  });
                });
              });



        })
    });
});
})(jQuery, this);

class Filter {
  constructor(
    selector,
    type = "select",
    multiple = false,
    placeholder = "",
    label = null,
    array = null
  ) {
    this.selector = selector;
    this.type = type;
    this.multiple = type === "select" ? multiple : null;
    this.placeholder = placeholder;
    this.element;
    this.label = label;
    if (array != null) {
      this.data = {
        name: selector,
        list: array,
      };
      this.createFilter();
    } else {
      this.getData(Filter.dataUrl);
    }
  }

  setOrder(i) {
    this.element.style.order = i;
  }

  // liveSearch(span){
  //   console.log('livesearch');
    
  //   span.addEventListener("keyup", function(){
  //     var inputValue = this.value, i;
  //     var filterList = document.getElementById(this.dataset.filter);
  //     var filterItem = this.querySelectorAll("li");
  //     for (i = 0; i < filterItem.length; i++) {
  //         var _this = filterItem[i];
  //         var phrase = _this.innerHTML; 
  //       if (phrase.search(new RegExp(inputValue, "i")) < 0) {
  //         _this.style.display = "none";
  //       } else {
  //         _this.style.display = "block";
  //       }
  //     }
  //   });
  // };


  // create filter input corresponding to given paramters
  createFilter() {
    this.element = document.createElement("div");
    this.element.classList.add("filtres");
    this.element.classList.add(this.selector);

    switch (this.type) {
      case "select": {
        if (this.label != null) {
          let label = this.setLabel();
          this.element.appendChild(label);
        }
        let element = this.selectInput();
        this.element.appendChild(element);
        break;
      }
      case "texte": {
        if (this.label != "") {
          let label = this.setLabel();
          this.element.appendChild(label);
        }
        let element = this.texteInput();
        this.element.appendChild(element);

        break;
      }
      default: {
        console.error("Invalid input type : " + this.type);
        break;
      }
    }

    Filter.parent.appendChild(this.element);
  }

  // create basic select input and wrap him in a lot of stuff to make it look pretty
  // If someone is trying to understand and use this function : wallah bonne chance
  // To sum up => create element into element into element that open other element and hide other element to select an option in other element
  //
  //    div @element - parent element
  //     ↳ div @active
  //        ↳ span @span - placeholder
  //        ↳ select @input  - classic select input
  //            ↳ option @opt - with class options in readonly
  //        ↳ a @n  - visual element represent selected option
  //     ↳ ul @list
  //        ↳ li @opt/li - option in list view
  //
  selectInput() {
    let input = document.createElement("select");
    input.setAttribute("name", this.selector);
    if (this.multiple) input.setAttribute("multiple", "multiple");
    if (!this.multiple) {
      let empty = document.createElement("option");
      empty.classList.add("empty");
      empty.setAttribute("value", "");
      input.appendChild(empty);
    }
    this.data.list.forEach((el) => {
      let opt = document.createElement("option");
      opt.innerHTML = el;
      opt.setAttribute("value", el);
      input.appendChild(opt);
    });

    let element = document.createElement("div");
    element.classList.add("selectMultiple");
    let span = document.createElement("input");
    span.classList.add('livesearch-input');
    span.setAttribute("placeholder", this.placeholder);  
    // span.innerHTML = this.placeholder;
    span.appendChild(input);
   


  // var inputFilter = document.querySelector("[data-filter]");
  span.addEventListener("keyup", function(){
  	var inputValue = this.value, i;
    var test = inputValue.normalize("NFD").replace(/\p{Diacritic}/gu, "")

    let placeholder = this.parentElement.nextElementSibling;
    let filterItem = placeholder.querySelectorAll('li');
    
    
    for (i = 0; i < filterItem.length; i++) {
    		var _this = filterItem[i];
        var phrase = _this.innerHTML.normalize("NFD").replace(/\p{Diacritic}/gu, "");
        
        console.log(phrase);
    	if (phrase.search(new RegExp(test, "gmi")) < 0) {
      	_this.style.display = "none";
      } else {
      	_this.style.display = "block";
      }
    }
  });


    let active = document.createElement("div");
    active.classList.add("search-input-parent");
    active.addEventListener("click", function () {
      let confirm = element.classList.contains("open") ? false : true;
      document
        .querySelectorAll(".selectMultiple.open:not(.deleting)")
        .forEach((o) => {
          o.classList.remove("open");
        });
      if (confirm) element.classList.add("open");
    });
    active.appendChild(span);
    let list = document.createElement("ul");
    list.classList.add("livesearch-list");

    this.data.list.forEach((el) => {
      // if on page load filters are already set (coming from homepage or generate link)
      if (
        Filter.currentSearch != null &&
        Filter.currentSearch[this.selector] !== undefined &&
        Filter.currentSearch[this.selector].includes(el)
      ) {
        let n = document.createElement("a");
        n.innerHTML = "<em>" + el + "</em><i></i>";
        span.classList.add("hide");
        n.addEventListener("click", function (e) {
          element.classList.add("deleting");
          n.classList.add("remove");
          document.querySelectorAll(".selectMultiple.open").forEach((o) => {
            o.classList.remove("open");
          });
          element.classList.add("open");
          setTimeout(function () {
            n.classList.add("disapear");
            setTimeout(function () {
              n.classList.remove("shown");
            }, 300);
            let li = element.querySelector("li.remove");
            let target = element.querySelector(`option[value='${el}']`);
            li.animate(
              {
                padding: "12px 16px",
                height: "auto",
              },
              { easing: "ease", fill: "forwards", duration: 300 }
            );
            setTimeout(function () {
              li.classList.remove("remove");
              target.removeAttribute("selected");
              n.remove();
              element.classList.remove("deleting");
              if (input.value == "") span.classList.remove("hide");
            }, 300);
          }, 200);
        });
        active.appendChild(n);
        setTimeout(() => {
          element
            .querySelector(`option[value='${el}']`)
            .setAttribute("selected", true);
        }, 100);
      }
      // end gloubiboulga

      let opt = document.createElement("li");
      opt.innerHTML = el;
      if (
        Filter.currentSearch != null &&
        Filter.currentSearch[this.selector] !== undefined &&
        Filter.currentSearch[this.selector].includes(el)
      ) {
        opt.classList.add("remove");
        opt.style.height = 0;
        opt.style.padding = 0;
      }
      // if input allow multiple option
      if (this.multiple === true) {
        // click to add event => remove option form list and put it in select input
        opt.addEventListener("click", (e) => {
          let li = e.target;
          if (!element.classList.contains("clicked")) {
            // these 2 lines force to wait previous call to totally end before make a new call
            element.classList.add("clicked");
            if (li.nextSibling != null)
              li.nextSibling.classList.add("beforeRemove");
            if (li.previousSibling != null)
              li.previousSibling.classList.add("afterRemove");
            li.classList.add("remove");
            let n = document.createElement("a");
            n.classList.add("notShown");
            n.innerHTML = "<em>" + li.innerHTML + "</em><i></i>";
            let target = element.querySelector(
              `option[value='${li.innerHTML}']`
            );
            target.setAttribute("selected", true);
            // click to remove event => Remove option from input and send him to the list
            n.addEventListener("click", (e) => {
              element.classList.add("deleting");
              n.classList.add("remove");
              document.querySelectorAll(".selectMultiple.open").forEach((o) => {
                o.classList.remove("open");
              });
              element.classList.add("open");
              setTimeout(function () {
                n.classList.add("disapear");
                setTimeout(function () {
                  n.classList.remove("shown");
                }, 300);

                li.animate(
                  {
                    padding: "12px 16px",
                    height: "auto",
                  },
                  { easing: "ease", fill: "forwards", duration: 300 }
                );
                setTimeout(function () {
                  li.classList.remove("remove");
                  target.removeAttribute("selected");
                  n.remove();
                  element.classList.remove("deleting");
                  if (input.value == "") span.classList.remove("hide");
                }, 300);
              }, 200);
            });
            // end click to remove event
            active.appendChild(n);
            setTimeout(function () {
              n.classList.add("shown");
              span.classList.add("hide");
              li.animate(
                {
                  height: 0,
                  padding: 0,
                },
                { easing: "ease", fill: "forwards", duration: 300 }
              );
            }, 500);
            setTimeout(function () {
              if (li.nextSibling != null)
                li.nextSibling.classList.remove("beforeRemove");
              if (li.previousSibling != null)
                li.previousSibling.classList.remove("afterRemove");
              element.classList.remove("clicked");
            }, 900);
          }
        });
        // end click to add event
      }
      // if input accept a single option
      else {
        opt.addEventListener("click", (e) => {
          let li = e.target;

          if (!element.classList.contains("clicked")) {
            element.classList.add("clicked");
            if (li.nextSibling != null)
              li.nextSibling.classList.add("beforeRemove");
            if (li.previousSibling != null)
              li.previousSibling.classList.add("afterRemove");

            let delay = 0;
            if (element.querySelector("a") !== null) {
              delay = 600;
              element.querySelector("a").classList.add("remove");
              setTimeout(() => {
                element.querySelector("a").remove();
                let lastli = element.querySelector("li.remove");
                lastli.animate(
                  {
                    padding: "12px 16px",
                    height: "auto",
                  },
                  { easing: "ease", fill: "forwards", duration: 300 }
                );

                setTimeout(() => {
                  lastli.classList.remove("remove");
                  let target = element.querySelector(
                    `option[value="${lastli.innerHTML}"]`
                  );
                  target.removeAttribute("selected");
                }, 300);
              }, 300);
            }
            setTimeout(() => {
              li.classList.add("remove");
              let n = document.createElement("a");
              n.classList.add("notShown");
              n.innerHTML = "<em>" + li.innerHTML + "</em><i></i>";
              let target = element.querySelector(
                `option[value="${li.innerHTML}"]`
              );
              target.setAttribute("selected", true);
              // click to remove event => Remove option from input and send him to the list
              n.addEventListener("click", (e) => {
                n.classList.add("remove");
                document
                  .querySelectorAll(".selectMultiple.open")
                  .forEach((o) => {
                    o.classList.remove("open");
                  });
                element.classList.add("open");
                setTimeout(function () {
                  n.classList.add("disapear");
                  setTimeout(function () {
                    n.classList.remove("shown");
                  }, 300);

                  li.animate(
                    {
                      padding: "12px 16px",
                      height: "auto",
                    },
                    { easing: "ease", fill: "forwards", duration: 300 }
                  );
                  setTimeout(function () {
                    li.classList.remove("remove");
                    target.removeAttribute("selected");
                    n.remove();
                    if (input.value == "") span.classList.remove("hide");
                  }, 300);
                }, 200);
              });
              // end click to remove event
              active.appendChild(n);
              setTimeout(function () {
                n.classList.add("shown");
                span.classList.add("hide");
                li.animate(
                  {
                    height: 0,
                    padding: 0,
                  },
                  { easing: "ease", fill: "forwards", duration: 300 }
                );
              }, 500);
            }, delay);

            setTimeout(function () {
              if (li.nextSibling != null)
                li.nextSibling.classList.remove("beforeRemove");
              if (li.previousSibling != null)
                li.previousSibling.classList.remove("afterRemove");
              element.classList.remove("clicked");
            }, 900);
          }
        });
      }
      list.appendChild(opt);
    });

    // add list's arrow
    let arrow = document.createElement("div");
    arrow.classList.add("arrow");
    active.appendChild(arrow);

    // add close air support brrrrr brrrrrr
    let trigger = document.createElement("div");
    trigger.classList.add("trigger");
    trigger.addEventListener("click", () => {
      document.querySelectorAll(".selectMultiple.open").forEach((o) => {
        o.classList.remove("open");
      });
    });

    element.appendChild(trigger);
    element.appendChild(active);
    element.appendChild(list);

    return element;
  }


  // template for search type input
  texteInput() {
    let element = document.createElement("input");
    element.setAttribute("type", "text");
    element.setAttribute("name", this.selector);
    element.setAttribute("placeholder", this.placeholder);
    element.setAttribute("id", "filter-" + this.selector);

    if (
      Filter.currentSearch != null &&
      Filter.currentSearch[this.selector] !== undefined
    ) {
      element.setAttribute("value", Filter.currentSearch[this.selector]);
    }

    element.addEventListener("click", () => {
      document.querySelectorAll(".selectMultiple.open").forEach((o) => {
        o.classList.remove("open");
      });
    });

    return element;
  }

  setLabel() {
    let label = document.createElement("label");
    label.innerHTML = this.label;
    label.setAttribute("for", this.selector);
    return label;
  }

  // Request to get csv file
  getData(url) {
    // If request isnt needed, save xhttp request
    if (this.type !== "search") {
      const xhttp = new XMLHttpRequest();
      xhttp.open("GET", url, true);
      xhttp.setRequestHeader("Content-type", "text");
      xhttp.send();
      const that = this;
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          that.data = that.csvJSON(this.responseText);
          that.createFilter();
        }
      };
    } else {
      this.createFilter();
    }
  }
  // get raw data from csv file and separate the column corresponding to selector
  csvJSON(csv) {
    const lines = csv.split("\r\n");
    const headers = lines[0].split(";");
    let index = headers.indexOf(this.selector);
    let target = headers[index];
    let list = [];
    for (let i = 1; i < lines.length; i++) {
      if (
        lines[i].split(";")[index] !== "" &&
        lines[i].split(";")[index] !== undefined
      ) {
        list[i] = lines[i].split(";")[index];
      }
    }
    let result = {
      name: target,
      list: list,
    };
    return result;
  }
}

("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {

      var isMobile = $(window).width() < 768 ? true : false;

      var map;
      var filtres;
      let coordinates = [];
      let cityvalues = [];

      $('#map').each(function () {
        if ($('#map').attr('db')) {
          map = new Maps({
            mapEl: document.getElementById('map'),
            dataUrl: $('#map').attr('db'),
            keyAPI: "AIzaSyB3jVJ4Bvh3JOxBDqfNazrxi70x8l_YUXg&maps_ids=1e7ba97c74c9d1b4",
            template: 'location-template',
          })
        }
        else {
          console.error('Centres: No file found !');
        }

        if ($('#map').attr('filtres')) {
          Filter.dataUrl = $('#map').attr('filtres');
          Filter.parent = document.querySelector('#filtre-input');
          Filter.currentSearch = map.search;
          filtres = {
            produits: new Filter('PRODUITS', 'select', true, 'Produits'),
            location: new Filter('CP', 'texte', null, 'Code postal ou dep.'),
            public: new Filter('PUBLIC', 'select', true, "Public", null),
            aide: new Filter('AIDE', 'select', false, "Type d'aide"),
            coord: setTimeout(() => {
              let coordList = [];
              map.centers.forEach(center => {
                coordList.push(center.COORD);
              });
              coordinates = [...coordList];
              let coordTest = new Filter('COORD', 'select', false, "Coordonnées", null, coordList);

              // setTimeout(() => {console.log(coordList) }, 600);
            }, 500),
            ville: setTimeout(() => {
              let cityList = [];
              map.centers.forEach((center, i) => {
                cityList.push(center.VILLE)[i];
              });
              for (let i = 0; i < cityList.length; i++) {
                cityvalues[i] = cityList[i];
              }

              // cityvalues = [...cityList, 1, 2];
              let citySort = cityList.sort((a, b) =>
                a.toLowerCase() > b.toLowerCase() ? 1 : -1
              ).filter((c, index) => { return cityList.indexOf(c) === index });
              let cityTest = new Filter('VILLE', 'select', false, "Nom de la ville", null, citySort);

            }, 500),
            nom: setTimeout(() => {
              let nomList = [];
              map.centers.forEach(center => {
                nomList.push(center.NOM);
              });
              let nomSort = nomList.sort((a, b) =>
                a.toLowerCase() > b.toLowerCase() ? 1 : -1
              ).filter((c, index) => { return nomList.indexOf(c) === index });

              let nomTest = new Filter('NOM', 'select', false, "Nom de l'établissement", null, nomSort);

            }, 500)
          }

        }
        else {
          console.error('Filtres: No file found !');
        }
      })

      setTimeout(() => {
        let cityCoordinates = cityvalues.reduce((acc, value, i) => (acc[value] = coordinates[i], acc), {}); 
        var sortedKeys = Object.keys(cityCoordinates).sort(); 




        $('.search-submit').each(function () {
          $(this).click(function () {
            const CP = $('input[name="CP"]');
            const city = $('.VILLE a em').html();
            cityCoordinates[city] ? coordinatesForPostalCode = cityCoordinates[city].split(',') : '';
            let request = {};
            $('.filtres').each(function () {
              let input = $(this).find('select')[0];
              if (input == undefined) input = $(this).find('input')[0];
              request[$(input).attr('name')] = $(input).val();
            });
            let postalCode;
            // console.log((CP.val()[0]) + (CP.val()[1]));
            // Ex: if string inserted in postal code input is 33 or starts with 33 --> zoom on { lat: 44.837789, lng: -0.579180 }
            if (city) {
              coordinatesForPostalCode ? postalCode = { lat: parseFloat(coordinatesForPostalCode[0]), lng: parseFloat(coordinatesForPostalCode[1]) } : '';
            } else {
              switch (CP.val() && (CP.val()[0]) + (CP.val()[1])) {
                case '33':
                  postalCode = { lat: 44.837789, lng: -0.579180 };
                  break;
                case '16':
                  postalCode = { lat: 45.648377, lng: 0.1562369 };
                  break;
                case '23':
                  postalCode = { lat: 45.954090, lng: 2.168938 };
                  break;
                case '19':
                  postalCode = { lat: 45.406018, lng: 2.046378 };
                  break;
                case '17':
                  postalCode = { lat: 45.744175, lng: -0.633389 };
                  break;
                case '24':
                  postalCode = { lat: 44.946890, lng: 0.808845 };
                  break;
                case '40':
                  postalCode = { lat: 43.893485, lng: -0.499782 };
                  break;
                case '47':
                  postalCode = { lat: 44.341894, lng: 0.393267 };
                  break;
                case '64':
                  postalCode = { lat: 43.327408, lng: -1.032999 };
                  break;
                case '79':
                  postalCode = { lat: 46.648825, lng: -0.251441 };
                  break;
                case '86':
                  postalCode = { lat: 46.580224, lng: 0.340375 };
                  break;
                case '87':
                  postalCode = { lat: 45.833619, lng: 1.261105 };
                  break;
                default:
                  postalCode = { lat: 44.393149, lng: -0.579180 };
              }
            }


            map.newSearch(request, postalCode,10);
            $("body").addClass("search-done");

          });
        });
      }, 700);

      $('.search-submit').click(function () {
        const CP = document.querySelector('input[name="CP"]');
      })


      document.addEventListener('keydown', function (e) {
        if ($('body').hasClass('page-template-template-map') || $('body').hasClass('home')) {
          if (e.key == "Enter") {
            let request = {};
            $('.filtres').each(function () {
              let input = $(this).find('select')[0];
              if (input == undefined) input = $(this).find('input')[0];
              request[$(input).attr('name')] = $(input).val();
            });
            map.newSearch(request);
            $("body").addClass("search-done");

          }
        }
      })

      // $('.rm-CP').click((e) => {
      //   const CP = document.querySelector('input[name="CP"]');
      //   $('#not-found-pic').attr('src', $('#theme_dir').html() + '/assets/img/ic_mood_bad_48px.svg');
      //   // If code postal is like 33000 keep only the two first digits, else remove the code postal
      //   const newCP = CP.value.length > 2 ? CP.value.substring(0, 2) : "";
      //   CP.value = newCP;

      //   map.updateCP(newCP);
      // });

      $('.rm-CP').click((e) => {
        const CP = document.querySelector('input[name="CP"]');
        $('#not-found-pic').attr('src', $('#theme_dir').html() + '/assets/img/ic_mood_bad_48px.svg');
        // If code postal is like 33000 keep only the two first digits, else remove the code postal
        const newCP = CP.value.length > 2 ? CP.value.substring(0, 2) : "";
        CP.value = newCP;
        map.updateCP(newCP, 9);

      });
      var timer = 1500;
      var loaderClass = $('.loader');
      setTimeout(() => {
        loaderClass.fadeOut();
        setTimeout(() => {
          loaderClass.remove();
        }, 1000)
      }, timer);

      /**
      *   FAQ GUTENBERG
      */
      $('.schema-faq-question').click(function () {
        $(this).parent().find('.schema-faq-answer').slideToggle(300);
        $(this).toggleClass('question-open');
      });


      /**
       * Burger menu toggle
       */
      $('.menu-toggle').each(function () {
        $(this).click(function () {
          $('body').toggleClass('nav-open');
          $('.mobile-nav-mask').fadeToggle();
        });
      });

      /**
       * responsive form map
       */
      if (isMobile) {
        $('.mobile-form').each(function () {
          $(this).click(function () {
            $('body').toggleClass('mobile-form-open');
            $('.filtre-container').slideToggle();
            $('.mobile-form-mask').fadeToggle();
            if ($('body').hasClass('mobile-form-open')) {
              $(this).text('fermer le formulaire');
            } else {
              $(this).text('Critères de recherche');
            }
          });
          $('.search-submit').click(function () {
            $('body').removeClass('mobile-form-open');
            $('.filtre-container').slideUp();
            $('.mobile-form-mask').fadeOut();
            $('.mobile-form').text('Critères de recherche');
          });
        });
      }
      //test pipeline 2
    });
  });
})(jQuery, this);

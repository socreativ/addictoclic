/*
 * Browser Detect script
 */
BrowserDetect = (function () {
    var options = {
      osVersion: true,
      minorBrowserVersion: true,
    };
  
    // browser data
    var browserData = {
      browsers: {
        chrome: uaMatch(/Chrome\/([0-9\.]*)/),
        firefox: uaMatch(/Firefox\/([0-9\.]*)/),
        safari: uaMatch(/Version\/([0-9\.]*).*Safari/),
        opera: uaMatch(/Opera\/.*Version\/([0-9\.]*)/, /Opera\/([0-9\.]*)/),
        msie: uaMatch(/MSIE ([0-9\.]*)/, /Trident.*rv:([0-9\.]*)/),
      },
      engines: {
        webkit: uaContains("AppleWebKit"),
        trident: uaMatch(/(MSIE|Trident)/),
        gecko: uaContains("Gecko"),
        presto: uaContains("Presto"),
      },
      platforms: {
        win: uaMatch(/Windows NT ([0-9\.]*)/),
        mac: uaMatch(/Mac OS X ([0-9_\.]*)/),
        linux: uaContains("X11", "Linux"),
      },
    };
  
    // perform detection
    var ua = navigator.userAgent;
    var detectData = {
      platform: detectItem(browserData.platforms),
      browser: detectItem(browserData.browsers),
      engine: detectItem(browserData.engines),
    };
  
    // private functions
    function uaMatch(regExp, altReg) {
      return function () {
        var result = regExp.exec(ua) || (altReg && altReg.exec(ua));
        return result && result[1];
      };
    }
    function uaContains(word) {
      var args = Array.prototype.slice.apply(arguments);
      return function () {
        for (var i = 0; i < args.length; i++) {
          if (ua.indexOf(args[i]) < 0) {
            return;
          }
        }
        return true;
      };
    }
    function detectItem(items) {
      var detectedItem = null,
        itemName,
        detectValue;
      for (itemName in items) {
        if (items.hasOwnProperty(itemName)) {
          detectValue = items[itemName]();
          if (detectValue) {
            return {
              name: itemName,
              value: detectValue,
            };
          }
        }
      }
    }
  
    // add classes to root element
    (function () {
      // helper functions
      var addClass = function (cls) {
        var body = document.body;
        body.className += (body.className ? " " : "") + cls;
      };
      var getVersion = function (ver) {
        return typeof ver === "string" ? ver.replace(/\./g, "_") : "unknown";
      };
  
      // add classes
      /*if (detectData.platform) {
        addClass(detectData.platform.name);
        if (options.osVersion) {
          addClass(
            detectData.platform.name + "-" + getVersion(detectData.platform.value)
          );
        }
      }
      if (detectData.engine) {
        addClass(detectData.engine.name);
      }*/
      if (detectData.browser) {
        addClass(detectData.browser.name);
        /*addClass(
          detectData.browser.name + "-" + parseInt(detectData.browser.value, 10)
        );
        if (options.minorBrowserVersion) {
          addClass(
            detectData.browser.name + "-" + getVersion(detectData.browser.value)
          );
        }*/
      }
    })();
  
    // export detection information
    return detectData;
  })();
  

  if (
    BrowserDetect.engine.name == "trident" || // Internet explorer
    (BrowserDetect.browser.name == "firefox" && parseInt(BrowserDetect.browser.value, 10) < 30) ||
    (BrowserDetect.browser.name == "chrome" && parseInt(BrowserDetect.browser.value, 10) < 36) ||
    (BrowserDetect.browser.name == "safari" && parseInt(BrowserDetect.browser.value, 10) < 8) ||
    (BrowserDetect.browser.name == "opera" && parseInt(BrowserDetect.browser.value, 10) < 30)
  ) {
    alert(
      "Navigateur obsolète. \nIl semble que vous exécutiez sur un très ancien navigateur Web, il est possible que la page s'affiche mal. Nous vous conseillons de mettre à jour votre navigateur ou d'en utiliser un plus modernes. Merci!"
    );
  }
  
  if (
    BrowserDetect.browser.name !== "firefox" && 
    BrowserDetect.browser.name !== "chrome" &&
    BrowserDetect.browser.name !== "safari" &&
    BrowserDetect.browser.name !== "opera"
  ) {
    alert(
      "Navigateur non reconnu. \nLe site peut ne pas s'afficher correctement."
    );
  }
  
  
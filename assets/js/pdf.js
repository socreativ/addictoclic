function generatePDF() {

    // Choose the element that your content will be rendered to.
    let element = document.getElementById('fiche-etablissement-modal');

    // Choose the element and save the PDF for your user.
    html2pdf().from(element).save();
}
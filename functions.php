<?php
/**
 * theme-by-socreativ functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme-by-socreativ
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'theme_by_socreativ_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function theme_by_socreativ_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on theme-by-socreativ, use a find and replace
		 * to change 'theme-by-socreativ' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'theme-by-socreativ', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'theme_by_socreativ_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'theme_by_socreativ_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_by_socreativ_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'theme_by_socreativ_content_width', 640 );
}
add_action( 'after_setup_theme', 'theme_by_socreativ_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_by_socreativ_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'theme-by-socreativ' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'theme-by-socreativ' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sub Footer', 'theme-by-socreativ' ),
			'id'            => 'sub-footer-widget',
			'description'   => esc_html__( 'Add widgets here.', 'theme-by-socreativ' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
		)
	);
}
add_action( 'widgets_init', 'theme_by_socreativ_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function theme_by_socreativ_scripts() {
	wp_enqueue_style( 'theme-by-socreativ-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'theme-by-socreativ-style', 'rtl', 'replace' );

	/** SoCreativ styles + bootstrap  **/
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.custom.min.css', true, '1.0', 'all');
	wp_enqueue_style( 'helpers', get_stylesheet_directory_uri() . '/assets/css/helpers.css', true, '1.0', 'all');
	wp_enqueue_style( 'gutenberg', get_stylesheet_directory_uri() . '/assets/css/gutenberg.css', true, '1.0', 'all');
	wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.css', true, '1.0', 'all');
	wp_enqueue_style( 'home', get_stylesheet_directory_uri() . '/assets/css/home.css', true, '1.0', 'all');
	wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.css', true, '1.0', 'all');
	wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', true, '1.0', 'all');
	wp_enqueue_style( 'maps', get_stylesheet_directory_uri() . '/assets/css/maps.css', true, '1.0', 'all');
	wp_enqueue_style( 'formulaire', get_stylesheet_directory_uri() . '/assets/css/formulaire.css', true, '1.0', 'all');

	/** Tweenmax + ScrollMagic   **/
	wp_enqueue_script( 'tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', ['jquery'], '1.0.0', true);
	//wp_enqueue_script( 'attrplugin.gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/AttrPlugin.min.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'ScrollToPlugin', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script( 'scrollmagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.min.js', ['jquery'], '1.0.0', true, false);
	//wp_enqueue_script( 'scrollmagicaddIndicators', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/debug.addIndicators.min.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'animation.gsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/animation.gsap.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script( 'html2pdf', 'https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js', ['jquery'], '1.0.0', true);
	//wp_enqueue_script( 'mousewheel', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js', array(), _S_VERSION, true );

	/** Google map */
	wp_enqueue_script('API-googleMap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB3jVJ4Bvh3JOxBDqfNazrxi70x8l_YUXg&maps_ids=1e7ba97c74c9d1b4', ['jquery'], '1.0.0', false);
	wp_enqueue_script('maps-cluster', 'https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js', ['jquery'], '1.0.0', true);
	wp_enqueue_script('maps-overlaping', 'https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js', array(), '1.0.0', true);

	/** SoCreativ scripts  **/
	wp_enqueue_script( 'CheckBrowser', get_template_directory_uri() . '/assets/js/browser.js', array(), '1.0.0', true);
	wp_enqueue_script( 'theme-by-socreativ-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'maps', get_template_directory_uri() . '/assets/js/maps.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'filters', get_template_directory_uri() . '/assets/js/filter.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'formulaire', get_template_directory_uri() . '/assets/js/formulaire.js', ['jquery'], '1.0.0', false);
	wp_enqueue_script( 'globalScript', get_template_directory_uri() . '/assets/js/script.js', ['jquery'], '1.0.0', false);
	
	wp_enqueue_script( 'pdf', get_template_directory_uri() . '/assets/js/pdf.js', ['jquery'], '1.0.0', false);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( is_page_template( 'template-parts/template-side-title.php' )) {
		wp_enqueue_style( 'side-title-css', get_stylesheet_directory_uri() . '/assets/css/side-title.css', true, '1.0', 'all');
	}
}
add_action( 'wp_enqueue_scripts', 'theme_by_socreativ_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * 	Create custom thumbnail size
 */

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'square-thumb', 400, 400, true ); // 300 pixels wide (and unlimited height)
}



/* -------------------------------------------
				GUTENBERG STYLE
--------------------------------------------- */

	// Gutenberg Editor width
	function gb_gutenberg_admin_styles() {
		echo '
			<style>
				/* Main column width */
				.wp-block {
					max-width: 1220px;
				}

				/* Width of "wide" blocks */
				.wp-block[data-align="wide"] {
					max-width: 1980px;
				}

				/* Width of "full-wide" blocks */
				.wp-block[data-align="full"] {
					max-width: none;
				}
			</style>
		';
	}
	add_action('admin_head', 'gb_gutenberg_admin_styles');

	// CUSTOM GUTENBERG FONT SIZE
	add_theme_support('editor-font-sizes',
	array(
		array(
			'name'      => __( 'Big Title', 'socreativ-theme' ),
			'shortName' => __( 'N', 'socreativ-theme' ),
			'size'      => 70,
			'slug'      => 'big'
		),
		array(
			'name'      => __( 'Big text', 'socreativ-theme' ),
			'shortName' => __( 'F', 'socreativ-theme' ),
			'size'      => 30,
			'slug'      => 'bigtext'
		),
		array(
			'name'      => __( 'small text', 'socreativ-theme' ),
			'shortName' => __( 'A', 'socreativ-theme' ),
			'size'      => 16,
			'slug'      => 'small'
		)
	)
	);

	// BUTTON STYLE
	register_block_style(
	'core/button',
		array(
		'name'  => 'light-link',
		'label' => __( 'Light Link', 'wp-light-link' ),
		)
	);
	register_block_style(
	'core/button',
		array(
		'name'  => 'addictoclic-link',
		'label' => __( 'addictoclic Link', 'wp-addictoclic-link' ),
		)
	);
	register_block_style(
	'core/button',
		array(
		'name'  => 'addictoclic-arrow-link',
		'label' => __( 'addictoclic Link + arrow', 'wp-addictoclic-arrow-link' ),
		)
	);
	// COLUMNS
	register_block_style(
	'core/columns',
		array(
		'name'  => 'columns-link',
		'label' => __( 'Columns Pictos Link', 'wp-columns-link' ),
		)
	);
	// COLUMN sqlite_fetch_single
	register_block_style(
	'core/column',
		array(
		'name'  => 'column-border',
		'label' => __( 'Column with border', 'wp-column-border' ),
		)
	);
	// GROUP
	register_block_style(
	'core/group',
	array(
		'name'  => 'container',
		'label' => __( 'group container on grid', 'wp-group-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-small',
			'label' => __( 'group container small center', 'wp-group-small-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-half-right',
			'label' => __( 'group container half right', 'wp-group-half-right-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-half-left',
			'label' => __( 'group container half left', 'wp-group-half-left-container' ),
		)
	);



	// GUTENBERG colors
	function mytheme_setup_theme_supported_features() {

		add_theme_support( 'editor-color-palette',
			array(
				array( 'name' => 'blue', 'slug'  => 'blue', 'color' => '#01416A' ),
				array( 'name' => 'green', 'slug'  => 'green', 'color' => '#61B492' ),
				array( 'name' => 'orange', 'slug'  => 'orange', 'color' => '#E2735C' ),
				array( 'name' => 'grey', 'slug'  => 'grey', 'color' => '#F6F6F6' ),
				array( 'name' => 'white', 'slug'  => 'white', 'color' => '#fff' ),
			)
		);
	}
	add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );
/*********************************************************************
                            Custom Menu
 ********************************************************************/

	// Create custom main menu
	function wpb_custom_new_menu() {
		register_nav_menus(
			array(
				'main-menu' => __( 'Main menu' ),
				'burger-menu' => __( 'Menu Burger' ),
				'footer' => __( 'Footer '),
				'RS' => __('Réseau Sociaux')
			)
		);
	}
	add_action( 'init', 'wpb_custom_new_menu' );

	// Get ACF image field and create new attribute to object
	add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
	function my_wp_nav_menu_objects( $items, $args ) {
		foreach( $items as &$item ) {
			$image = get_field('menu_image', $item);
			$icon = get_field('menu_icon', $item);
			if( $image ) {
				$item->attr_img = $image['url'];
				$item->attr_img_item = 'image-item';
			}
			if( $icon ) {
				$item->icon = '<img class="" src="'. $icon['url'] . '" alt="' . $icon['alt'] . '"/>';
			}
			$item->title = $item->icon . '<span class="anim-300">' . $item->title . '</span>';
		}
		return $items;
	}



	// If object had new 'img' attribute, insert it into anchor element
	add_filter( 'nav_menu_link_attributes', 'wpse_100726_extra_atts', 10, 3 );
	function wpse_100726_extra_atts( $atts, $item, $args ){
			$atts['data-img'] = $item->attr_img;
			$atts['data-item'] = $item->attr_img_item;
			return $atts;
	}

	// Add
	add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
	function add_admin_link($items, $args){
		if( $args->theme_location == 'footer_menu' ){
			$items .= '<li><a title="Admin" href="'. esc_url( admin_url() ) .'">' . __( 'Admin' ) . '</a></li>';
		}
		return $items;
	}

/***************************************************
					ACF Params
****************************************************/


	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' => __('Options'),
			'menu_title' => __('Options'),
			'menu_slug' => 'options',
			'position' => '31'
		));
	}

	add_action('acf/init', 'my_acf_init_block_types');
	function my_acf_init_block_types() {

		/* Add gategorie for blocks */
		function my_block_category($categories, $post){
			return array_merge(
				$categories,
				array(
					array(
						'slug' => 'custom-blocks',
						'title' => __('Custom Block', 'custom-blocks'),
					),
				)
			);
		}
		add_filter('block_categories', 'my_block_category', 10, 2);

		//custom block ACF
		require 'inc/customBlocks.php';

	}

/* -------------------------------------------
				Custom functions
--------------------------------------------- */


	// CUSTOM WP_IS_MOBILE
	function my_wp_is_mobile() {
		static $is_mobile;

		if ( isset($is_mobile) )
			return $is_mobile;

		if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
			$is_mobile = false;
		} elseif (
			strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
				$is_mobile = true;
		} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
				$is_mobile = true;
		} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
			$is_mobile = false;
		} else {
			$is_mobile = false;
		}

		return $is_mobile;
	}

	function add_cors_http_header(){
		header("Access-Control-Allow-Origin: *");
	}
	add_action('init','add_cors_http_header');






	// Allow SVG
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
     return $data;
  }

  $filetype = wp_check_filetype( $filename, $mimes );

  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() {
  echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'fix_svg' );



// Send email

function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );


function sc_send_mail(){

	require_once get_template_directory() . '/template-parts/template-email.php';

	// Set content-type header for sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	$func = $_POST['func'];

	$from = new stdClass;
	$from->FULLNAME = $_POST['from_name'];
	$from->MAIL = $_POST['from_mail'];
	$from->TEL = $_POST['from_tel'];
	$from->POSTE = $_POST['from_poste'];

	$center = new stdClass;
	$center->NOM = $_POST['NOM'];
	$center->ACR = $_POST['ACR'];
	$center->ACTIVITE = $_POST['ACTIVITE'];
	$center->AIDE = $_POST['AIDE'];
	$center->EQUIPE = $_POST['EQUIPE'];
	$center->INFO = $_POST['INFO'];
	$center->PRODUITS = $_POST['PRODUITS'];
	$center->PUBLIC = $_POST['PUBLIC'];
	$center->STRUCTURE = $_POST['STRUCTURE'];
	$center->PRIX = $_POST['PRIX'];
	$center->RDV = $_POST['RDV'];

	$center->ADRESSE = $_POST['ADRESSE'];
	$center->COORD = $_POST['COORD'];
	$center->CP = $_POST['CP'];
	$center->CPL = $_POST['CPL'];
	$center->DEP = $_POST['DEP'];
	$center->FAX = $_POST['FAX'];
	$center->HORAIRES = $_POST['HORAIRES'];
	$center->MAIL = $_POST['MAIL'];
	$center->TEL1 = $_POST['TEL1'];
	$center->TEL2 = $_POST['TEL2'];
	$center->VILLE = $_POST['VILLE'];
	$center->WEB = $_POST['WEB'];

	$data = new stdClass;
	$data->CENTER = $center;
	$data->FROM = $from;
	$data->func = $func;


	//$admin = get_field('mail', 'options');
	//$admin = get_option('admin_email');
	if (get_field('emails_admin_form','option')) {
		$admin = get_field('emails_admin_form','option');
	}else {
		$admin = get_option('admin_email');
	}

	$subject = $func === "Edit" ? "📯 | Addictoclic - Nouvelle demande d'édition" : "📯 | Addictoclic - Nouvelle demande d'ajout" ;
	if(wp_mail($admin, $subject, admin_mail($data), $headers)){
		echo "mail sent";
		$subject = "Addictoclic - Message envoyé";
		wp_mail($from->MAIL, $subject, public_mail($data), $headers);
	}
	else{
		echo "Error, mail not sent";
	}

	die();
}

add_action( 'wp_ajax_sendMail', 'sc_send_mail' );
add_action( 'wp_ajax_nopriv_sendMail', 'sc_send_mail' );

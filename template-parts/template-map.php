<?php
/**
* Template Name: Maps
*
* @package WordPress
*/

get_header(null, array('no-scroll'));
?>



		<?php
		while ( have_posts() ) :
			the_post(); ?>

			<div <?php post_class('app'); ?> id="page-<?php the_ID(); ?>">
				<div class="left-side scroll-col" id="myUL">
					<div class="search-count">
						<p id="list-count"><span>0</span> Résulat(s)</p>
						<a href="#" class="rm-CP">Élargir la recherche</a>
						<a href="<?= get_permalink(); ?>">
							<span>Réinitialiser <?php if (!my_wp_is_mobile()) {?>les criteres <?php } ?></span>
							<svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								<title>réinitialiser les critères</title>
								<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<g id="MAP" transform="translate(-506.000000, -216.000000)" fill="#E2735C" fill-rule="nonzero">
										<g id="ic_refresh_24px" transform="translate(506.250000, 216.250000)">
											<path d="M6.74562226,5.68434189e-14 C8.55841231,5.68434189e-14 10.2517568,0.719886122 11.5157426,1.98308195 L11.692464,2.15969292 L13.5,0.352 L13.5,5.625 L8.223,5.625 L10.793282,3.05831302 L10.617966,2.88148431 C9.58928438,1.84392678 8.21782695,1.25 6.74562226,1.25 C3.70952476,1.25 1.24233896,3.71564381 1.24233896,6.75 C1.24233896,9.78435619 3.70952476,12.25 6.74562226,12.25 L6.98758635,12.2447263 C9.15542361,12.1500931 11.047476,10.7898038 11.8442845,8.80599582 L11.865,8.75 L13.181,8.75 L13.1267494,8.92306273 C12.2094754,11.6268417 9.67063166,13.5 6.74562226,13.5 C3.01550749,13.5 0,10.4811633 0,6.75 C0,3.01883668 3.01550749,5.68434189e-14 6.74562226,5.68434189e-14 Z" id="Path"></path>
										</g>
									</g>
								</g>
							</svg>
						</a>
					</div>


				<div class="empty-center">

					<img id="not-found-pic" src="<?= get_template_directory_uri() . '/assets/img/no-results.svg' ?>">

					<h3>Oups</h3>
					<p>Aucun résultats pour cette recherche... </p>

					<a href="#" class="rm-CP" style="display:none;">
						Élargir la recherche autour de moi
					</a>

					<a href="<?= get_permalink(); ?>" class="reset" style="display:none;">
						Réinitialiser les critères

						<svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<title>réinitialiser les criteres</title>
							<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="MAP" transform="translate(-506.000000, -216.000000)" fill="#E2735C" fill-rule="nonzero">
									<g id="ic_refresh_24px" transform="translate(506.250000, 216.250000)">
										<path d="M6.74562226,5.68434189e-14 C8.55841231,5.68434189e-14 10.2517568,0.719886122 11.5157426,1.98308195 L11.692464,2.15969292 L13.5,0.352 L13.5,5.625 L8.223,5.625 L10.793282,3.05831302 L10.617966,2.88148431 C9.58928438,1.84392678 8.21782695,1.25 6.74562226,1.25 C3.70952476,1.25 1.24233896,3.71564381 1.24233896,6.75 C1.24233896,9.78435619 3.70952476,12.25 6.74562226,12.25 L6.98758635,12.2447263 C9.15542361,12.1500931 11.047476,10.7898038 11.8442845,8.80599582 L11.865,8.75 L13.181,8.75 L13.1267494,8.92306273 C12.2094754,11.6268417 9.67063166,13.5 6.74562226,13.5 C3.01550749,13.5 0,10.4811633 0,6.75 C0,3.01883668 3.01550749,5.68434189e-14 6.74562226,5.68434189e-14 Z" id="Path"></path>
									</g>
								</g>
							</g>
						</svg>
					</a>


				</div>

				<div id="location-template">
					<div class="location-container">
						<h3 class="loc-title">Titre</h3>
						<p class="loc-city">City</p>
						<p class="loc-dep">Département</p>
						<p class="loc-tel">Tél</p>
						<a class="loc-mail" href="mailto:#">Mail</a>
					</div>
					<?php if (!my_wp_is_mobile()) {?>
						<div class="location-arrow"></div>
					<?php } ?>
				</div>
				<?php if (my_wp_is_mobile()) {?>
					<div class="pr-3"></div>
				<?php } ?>
			</div>
			<div class="right-side">
				<div id="map" db="<?= get_field('csv', 'options')['centres']['url']; ?>" filtres="<?= get_field('csv', 'options')['filtres']['url']; ?>"></div>
			</div>

		</div>


		<div class="modal fade" tabindex="-1" role="dialog" style="display: none;">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div id="fiche-etablissement-modal" class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="modal-title">Modal title</h3>
						<button type="button" class="close exit" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12 col-md-6">
								<h4>Informations : </h4>
								<p class="loc-type">Type : <span></span></p>
								<p class="loc-act">Activité : <span></span></p>
								<p class="loc-aide">Aide : <span></span></p>
								<p class="loc-prod">Produit : <span></span></p>
								<p class="loc-team">Équipe : <span></span></p>
								<p class="loc-info">Infos complémentaires : <span></span></p>
								<p class="loc-public">Public : <span></span></p>
								<p class="loc-rdv">RDV : <span></span></p>

							</div>
							<div class="col-12 col-md-6">
								<h4>Coordonnées : </h4>
								<p class="loc-adresse"><span></span></p>
								<p class="loc-tel1"><img src="<?= get_template_directory_uri() . '/assets/img/phone-2.svg' ?>" /> <span></span></p>
								<p class="loc-tel2"><img src="<?= get_template_directory_uri() . '/assets/img/phone-2.svg' ?>" /> <span></span></p>
								<p class="loc-mail"><img src="<?= get_template_directory_uri() . '/assets/img/ic_email_24px.svg' ?>" /> <span></span></p>
								<p class="loc-web"><img src="<?= get_template_directory_uri() . '/assets/img/link-72.svg' ?>" /> <span></span></p>
								<p class="loc-fax"><img src="<?= get_template_directory_uri() . '/assets/img/fax.svg' ?>" /> <span></span></p>

								<h4>Horaires :</h4>
								<p class="loc-hor"><span></span></p>
							</div>
						</div>

						<hr>


					</div>
					<div class="modal-footer">
						<div class="left">
							<button href="#" class="btn print-btn">
								<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>ic_local_printshop_48px</title>
									<g class="nc-icon-wrapper" fill="#ffffff">
										<path d="M38 16H10c-3.31 0-6 2.69-6 6v12h8v8h24v-8h8V22c0-3.31-2.69-6-6-6zm-6 22H16V28h16v10zm6-14c-1.11 0-2-.89-2-2s.89-2 2-2c1.11 0 2 .89 2 2s-.89 2-2 2zM36 6H12v8h24V6z"/>
									</g>
								</svg>
								Imprimer cette fiche
							</button>
							<a href="#" class="modify ml-3">Modifier cette fiche ?</a>
						</div>
						<button type="button" class="btn pdf" onclick="generatePDF()">Télécharger la fiche</button>
						<button type="button" class="btn exit" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>



		<?php endwhile; // End of the loop.
		?>



<?php
get_footer(null, array('no-footer'));

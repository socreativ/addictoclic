<!-- SLIDER BEGIN -->


<?php 
function url_origin( $s, $use_forwarded_host = false )
{
    $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
    $sp       = strtolower( $s['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
    $port     = $s['SERVER_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
    $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
} 

function full_url( $s, $use_forwarded_host = false )
{
    return url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
}

$absolute_url = full_url( $_SERVER );
$domain = strstr(strval($absolute_url), '=');
$without_diez = $domain = ltrim($domain, '=');
?>




<section id="<?php echo $block['anchor']; ?>" class="<?php echo $block['className']; ?> position-relative glossaire">


  <div class="left-side">
    <?php if (!my_wp_is_mobile()) { ?>
      <img src="<?= get_template_directory_uri() . '/assets/img/bkg-top.svg' ?>" id="shape-pro-1">
    <?php } ?>

      <div class="title-left-content">
        <?php if (my_wp_is_mobile()) { ?>
          <div class="d-flex w-100 justify-content-between align-items-center p-3">
        <?php } ?>
        <h1>Glossaire</h1>
        <div class="d-flex align-items-center align-items-md-start flex-md-column">
          <?php if (my_wp_is_mobile()) { ?>
            <label for="text-search">
              <svg class="mr-3" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" xml:space="preserve" width="24" height="24"><title>zoom 2</title><g class="nc-icon-wrapper" stroke-linecap="square" stroke-linejoin="miter" stroke-width="2" fill="#e2735b" stroke="#e2735b"><line data-color="color-2" fill="none" stroke-miterlimit="10" x1="22" y1="22" x2="16.4" y2="16.4"/> <circle fill="none" stroke="#e2735b" stroke-miterlimit="10" cx="10" cy="10" r="9"/></g></svg>
            </label>
          <?php }else { ?>
            <label for="text-search">Tapez votre recherche ici</label>
          <?php } ?>
          <input type="text" id="text-search" class="text-search" data-source="#glossario" data-alphabet="#alphabet" data-searchtitle="h3" data-offset="300" value="<?php echo(isset($without_diez))?$without_diez:'';?>"/>
        </div>
        <?php if (my_wp_is_mobile()) { ?>
        </div>
        <?php } ?>
        <?php if (!my_wp_is_mobile()) { ?>
          <h3 class="mt-4">Tri par lettre</h3>
        <?php } ?>
        <div class="alphabet" id="alphabet">
        </div>
      </div>
      <?php if (!my_wp_is_mobile()) { ?>
        <img src="<?= get_template_directory_uri() . '/assets/img/bkg-bottom.svg' ?>" id="shape-pro-2">
      <?php } ?>
  </div>


  <div class="right-side">


      <div class="glossario" id="glossario">
        <?php if(get_field('a')) { ?>
          <div class="letter" id="A">
            <h2>a</h2>
            <?php the_field('a'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('b')) { ?>
          <div class="letter" id="B">
            <h2>b</h2>
            <?php the_field('b'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('c')) { ?>
          <div class="letter" id="C">
            <h2>c</h2>
            <?php the_field('c'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('d')) { ?>
          <div class="letter" id="D">
            <h2>d</h2>
            <?php the_field('d'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('e')) { ?>
          <div class="letter" id="E">
            <h2>e</h2>
            <?php the_field('e'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('f')) { ?>
          <div class="letter" id="F">
            <h2>f</h2>
            <?php the_field('f'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('g')) { ?>
          <div class="letter" id="G">
            <h2>g</h2>
            <?php the_field('g'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('h')) { ?>
          <div class="letter" id="H">
            <h2>h</h2>
            <?php the_field('h'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('i')) { ?>
          <div class="letter" id="I">
            <h2>i</h2>
            <?php the_field('i'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('j')) { ?>
          <div class="letter" id="J">
            <h2>j</h2>
            <?php the_field('j'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('k')) { ?>
          <div class="letter" id="K">
            <h2>k</h2>
            <?php the_field('k'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('l')) { ?>
          <div class="letter" id="L">
            <h2>l</h2>
            <?php the_field('l'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('m')) { ?>
          <div class="letter" id="M">
            <h2>m</h2>
            <?php the_field('m'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('n')) { ?>
          <div class="letter" id="N">
            <h2>n</h2>
            <?php the_field('n'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('o')) { ?>
          <div class="letter" id="O">
            <h2>o</h2>
            <?php the_field('o'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('p')) { ?>
          <div class="letter" id="P">
            <h2>p</h2>
            <?php the_field('p'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('q')) { ?>
          <div class="letter" id="Q">
            <h2>q</h2>
            <?php the_field('q'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('r')) { ?>
          <div class="letter" id="R">
            <h2>r</h2>
            <?php the_field('r'); ?>
          </div>
        <?php } ?>


        <?php if(get_field('s')) { ?>
          <div class="letter" id="S">
            <h2>s</h2>
            <?php the_field('s'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('t')) { ?>
          <div class="letter" id="T">
            <h2>t</h2>
            <?php the_field('t'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('u')) { ?>
          <div class="letter" id="U">
            <h2>u</h2>
            <?php the_field('u'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('v')) { ?>
          <div class="letter" id="V">
            <h2>v</h2>
            <?php the_field('v'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('w')) { ?>
          <div class="letter" id="W">
            <h2>w</h2>
            <?php the_field('w'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('x')) { ?>
          <div class="letter" id="X">
            <h2>x</h2>
            <?php the_field('x'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('y')) { ?>
          <div class="letter" id="Y">
            <h2>y</h2>
            <?php the_field('y'); ?>
  				</div>
        <?php } ?>


        <?php if(get_field('z')) { ?>
          <div class="letter" id="Z">
            <h2>z</h2>
            <?php the_field('z'); ?>
  				</div>
        <?php } ?>

			</div>
  </div>
</section>

("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {



      // ========= Search in text ==========
  // Utilizzo: creare un div contenente il glossario ed un <div> con classe "text-search" che supporta i seguenti data:
  // data-source = id_elem_glossario [obbligatorio] : è il testo da filtrare
  // data-alphabet = id_elem_alfabeto : è il div che contiene le lettere dell'alfabeto
  // data-searchtitle = elemento titolo da ricercare (se omesso viene cercato in tutto il testo [beta])
  // ===================================

  // Filtro per data-key="value"
  $.fn.filterData = function(key, value) {
    return this.filter(function() {
      return $(this).data(key) == value;
    });
  };
  // :contains case-insensitive
  $.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
      return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
  });
  // reimposta alfabeto
	function resetAlphabet(ab, letter) {
    var starter = (letter == '') ? $(ab).children('button').first().data('value') : letter;
    var lettercode = starter.charCodeAt(0);
    if ((lettercode >= 65) && (lettercode <= 90) && (ab != '')) {
      activateLetter($(ab).children('button').filterData("value", starter));
    }
  }
  // attiva div lettera
  function activateLetter(ellet) {
    var thisletter = $(ellet).data('value');
    window.scrollTo(0, 0);
    $(ellet).addClass('active');
    $(ellet).siblings().removeClass('active');
    if (!$("#" + thisletter).hasClass('active')) {
      $(source).children('div').removeClass('active');
      $("#" + $(ellet).data('value')).addClass('active');
    }
  }
  // delay di scrittura
  function delay(callback, ms) {
    var timer = 0;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
  }

  // Startup
	var $ts = $('.text-search');
	var source = '';
	var alphabet = '';
  var searchtitle = '';
  var searchin = '';
  var lbl = '';
	var $elemsfound;
  var offset = 0;

	if ($ts.length > 0) {
		alphabet = $ts.data('alphabet') || alphabet;
		source = $ts.data('source') || source;
    searchtitle = $ts.data('searchtitle') || searchtitle;
    lbl = $("label[for='" + $ts.attr('id') + "']");
    offset = $ts.data('offset') || 0;
    offset = ((searchtitle != '') && offset == 0)  ? parseInt($(searchtitle).css('marginTop')) : offset;

		if ($(source).length == 1) {
      if (alphabet != '') {
        var $ab = $(alphabet);
        if ($ab.length > 0) {
          var alphaletters = $(source).children('div');
          // Rimuovo eventuali colori di sfondo e bordi presenti x aiutare l'editing
          $(alphaletters).css('backgroundColor','').css('border','');
          $ab.html('');
          $(alphaletters).each(function(id, elem){
            $ab.append('<button type="button" data-value="' + $(elem).attr('id') + '">' + $(elem).attr('id') + '</button>');
          });
          $ab.on('click', 'button', function(e){
            e.preventDefault();
            e.stopPropagation();
            $('.text-search').val('');
            activateLetter($(this))
          });
          resetAlphabet(alphabet, '');
        } else {
          alphabet = '';
          $(source).children('div').addClass('active');
        }
      } else {
        $(source).children('div').addClass('active');
      }

			$('body').on('keyup', '.text-search', delay(function(e){
				var sRicerca = $(this).val();
				if (sRicerca != '') {
          var fl = sRicerca.substring(0, 1).toUpperCase();
          $(searchin + '.found').removeClass('found');
          searchin = source + ((alphabet == '') ? '' : ' > div#' + fl) + ((searchtitle == '') ? '' : ' ' + searchtitle);
					//console.log('Ricerco parola: "' + sRicerca + '" in "' + searchin + '"');
					$elemsfound = $(searchin + ':contains("' + sRicerca + '")');
          //console.log('elementi trovati: ' + $elemsfound.length);
          if ($elemsfound.length > 0) {
            resetAlphabet(alphabet, fl);
            if (searchtitle != '') {
              var goto = $elemsfound.first().offset().top - offset;
              $("html,body").animate({ scrollTop: goto}, 500 );
            } else {
              // TODO:  {... impostare goto per andare al testo selezionato .. }
              //https://stackoverflow.com/questions/10442513/how-to-wrap-part-of-the-text-with-spans-using-jquery
              //console.log('è una ricerca globale')
              $(source).html().replace(sRicerca, '<span class="found">' + sRicerca + '</span>');
            }
          } else {
            searchin = source + ((alphabet == '') ? '' : ' > div') + ((searchtitle == '') ? '' : ' ' + searchtitle);
            //console.log('NON HO TROVATO NULLA quindi ricerco parola: "' + sRicerca + '" in "' + searchin + '"');
            $elemsfound = $(searchin + ':contains("' + sRicerca + '")');
            if ($elemsfound.length > 0) {
              fl = $elemsfound.first().parent().attr('id');
              resetAlphabet(alphabet, fl);
              searchin = source + ((alphabet == '') ? '' : ' > div#' + fl) + ((searchtitle == '') ? '' : ' ' + searchtitle);
              $elemsfound = $(searchin + ':contains("' + sRicerca + '")');
              var goto = $elemsfound.first().offset().top - offset;
              //console.log(goto);
              $("html,body").animate({ scrollTop: goto}, 500 );
            }
          }
          $elemsfound.addClass('found');
          if (lbl.length == 1) {lbl.html( $elemsfound.length + ' element' + ($elemsfound.length == 1 ? '' : 's'))};
				} else {
          resetAlphabet(alphabet, '');
          $(searchin).removeClass('found');
          if (lbl.length == 1) {
            var windowWidth = $( window ).width();
            if ( windowWidth <= 576) {
              lbl.html('<svg class="mr-3" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" xml:space="preserve" width="24" height="24"><title>zoom 2</title><g class="nc-icon-wrapper" stroke-linecap="square" stroke-linejoin="miter" stroke-width="2" fill="#e2735b" stroke="#e2735b"><line data-color="color-2" fill="none" stroke-miterlimit="10" x1="22" y1="22" x2="16.4" y2="16.4"/> <circle fill="none" stroke="#e2735b" stroke-miterlimit="10" cx="10" cy="10" r="9"/></g></svg>')
            }else {
              lbl.html('Tapez votre recherche ici')
            }
          }
        }
			}, 500));
		}
	}

    }); // END DOMREADY
  });
})(jQuery, this);

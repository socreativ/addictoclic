<?php

if(my_wp_is_mobile()){
    $margin = get_field('mobile');
}
else{
    $margin = get_field('desktop');
}


?>

<div id="<?php echo $block['anchor']; ?>" class="spacer-block" style="height: <?php echo $margin; ?>vh;"></div>
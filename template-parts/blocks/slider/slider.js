// ("use strict");
// (function ($, root, undefined) {
//   $(function () {
//     $(document).ready(function () {
//       const homUrl = window.location.host;

//       $('.home-form').each(function (){
//         Filter.dataUrl = $('.home-form').attr('data-filter');
//         Filter.parent = document.querySelector('.home-form');
//         filtres = {
//           produits: new Filter('PRODUITS', 'select', true, 'Produits'),
//           location: new Filter('CP', 'texte', null, 'Code postal ou dep.'),
//           nom : new Filter('NOM', 'select', false, "Choisissez un établissement", null, null),
//         }
//         setTimeout(() => {
//           let i=0;
//           for (const key in filtres) {
//             if (Object.hasOwnProperty.call(filtres, key)) {
//               filtres[key].setOrder(i);
//               i++;
//             }else{
//               filtres[key].setOrder(-1)
//             }
//           }
//         }, 500)


//         // LOADER
//         var timer = 500;
//         var loaderClass = $('.loader-form-home');
//         var formClass = $('.home-form');

//         setTimeout(() => {
//           loaderClass.fadeOut();
//           formClass.css('opacity', '1');
//           setTimeout(() => {
//             loaderClass.remove();
//           }, 1000)
//         }, timer)


//         // CLICK
//         $( ".home-search-submit" ).click(function() {
//           let request = {};
//           $('.filtres').each(function (){
//             let input = $(this).find('select')[0];
//             if(input == undefined) input = $(this).find('input')[0];
//             request[$(input).attr('name')] = $(input).val();
//           });
//           let param = "";
//           let i=0;
//           for (const [n, v] of Object.entries(request)) {
//             if(v != "" && v.length != 0){
//               if(i>0) param += '&';
//               param += `${n}=${v}`;
//               i++;
//             }
//           }
          
//           window.location.href = homUrl + `/03-coreadd/annuaire?${param}`;
//         });


//       }); // end EACH


//     }); // END DOMREADY
//   });
// })(jQuery, this);



("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {
      const homUrl = window.location.host;

      $('.home-form').each(function (){
        
        Filter.dataUrl = $('.home-form').attr('data-filter');
        Filter.parent = document.querySelector('.home-form');
        const dataURL = $('.home-form').attr('data-filter');

        // Transform raw data from csv in javascript array
        function csvJSON(csv){
          var lines=csv.split("\r\n");
          var result = [];
          var headers=lines[0].split(";");
          for(var i=1;i<lines.length-1;i++){ // -1 to delete last blank caracter which create new line
              var obj = {};
              var currentline=lines[i].split(";");
              for(var j=0;j<headers.length;j++){
                obj[headers[j]] = currentline[j];
                obj["ID"] = i-1;
              }
              result.push(obj);
          }
          return result;
        }

        filtres = {
          produits: setTimeout(() => {
            getData(dataURL)
            function getData(url){
              const xhttp = new XMLHttpRequest();
              xhttp.open('GET', url, true);
              xhttp.setRequestHeader("Content-type", "text");
              xhttp.send();
              xhttp.onreadystatechange = function(e) {
                if(this.readyState == 4 && this.status == 200){
                  data = csvJSON(this.responseText);
                  let productlist = [];
                  data.forEach(e => {
                    productlist.push(e.PRODUITS.split(",")[0]);
                  });
                  let productSort = productlist.sort((a, b) =>
                      a.toLowerCase() > b.toLowerCase() ? 1 : -1
                      ).filter((c, index) => { return productlist.indexOf(c)=== index});
                  new Filter('PRODUITS', 'select', true, 'Produits', null, productSort);
                }
                else if(this.status == 404){
                  console.error('File not found !');
                }
              }
            }

          }, 500),
          location: new Filter('CP', 'texte', null, 'Code postal ou dep.')
        }
        setTimeout(() => {
          let i=0;
          for (const key in filtres) {
            console.log(filtres)
            if (Object.hasOwnProperty.call(filtres, key)) {
              console.log(filtres);
              filtres[key].setOrder(i);
              i++;
            }
          };
        }, 500)


        // LOADER
        var timer = 500;
        var loaderClass = $('.loader-form-home');
        var formClass = $('.home-form');

        setTimeout(() => {
          loaderClass.fadeOut();
          formClass.css('opacity', '1');
          setTimeout(() => {
            loaderClass.remove();
          }, 1000)
        }, timer)


        // CLICK
        $( ".home-search-submit" ).click(function() {
          let request = {};
          $('.filtres').each(function (){
            let input = $(this).find('select')[0];
            if(input == undefined) input = $(this).find('input')[0];
            request[$(input).attr('name')] = $(input).val();
          });
          let param = "";
          let i=0;
          for (const [n, v] of Object.entries(request)) {
            if(v != "" && v.length != 0){
              if(i>0) param += '&';
              param += `${n}=${v}`;
              i++;
            }
          }
          
          window.location.href = homUrl + `/03-coreadd/annuaire?${param}`;
        });


      }); // end EACH


    }); // END DOMREADY
  });
})(jQuery, this);

<!-- SLIDER BEGIN -->
<section id="<?php echo $block['anchor']; ?>" class="slider <?php echo $block['className']; ?> container-fluid position-relative">
  <img class="image-right-slider" src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/shape-slider.svg" alt="forme fond slider">
  <img class="image-bottom-slider" src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/dots.svg" alt="forme fond slider">
  <div class="">
    <div class="container">
      <div class="row">
        <?php if (!my_wp_is_mobile()) {?>
          <div class="d-none d-lg-flex col-md-4 col-lg-5 col-xl-6 ">
            <?php
            $image = get_field('imageleft');
            if ( !empty($image) ): ?>
            <img class="image-left" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
        <?php } ?>
        <div class="col-md-8 col-lg-7 col-xl-6 p-0 p-md-3">
          <p>
            <?php the_field( "texte_au_dessus_du_titre_principal" ); ?>
          </p>
          <h1>
            <?php the_field( "titre_principal" ); ?>
          </h1>
          <div class="separator"></div>
          <h2 class="mb-4">
            <?php the_field( "sous_titre" ); ?>
          </h2>
          <?php if (get_field('formtruefalse')) {?>
            <div class="loader-container-home">
              <div class="loader-form-home">
                <div class="loader">
                    <div class="lds-ripple">
                        <div></div>
                        <div></div>
                    </div>
                </div>
              </div>
              <div class="home-form anim-300" data-filter="<?= get_field('csv','option')['centres']['url'] ?>">
                <button class="home-search-submit mr-0"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve" width="32" height="32"><title>zoom 2</title><g class="nc-icon-wrapper" stroke-linecap="square" stroke-linejoin="miter" stroke-width="2" fill="#ffffff" stroke="#ffffff"><line data-cap="butt" fill="none" stroke="#ffffff" stroke-miterlimit="10" x1="30" y1="30" x2="21.5" y2="21.5" stroke-linecap="butt"/> <circle fill="none" stroke="#ffffff" stroke-miterlimit="10" cx="13" cy="13" r="12"/></g></svg></button>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>

</section>

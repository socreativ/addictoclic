<?php
/**
* Template Name: Titre à gauche
*
* @package WordPress
*/

get_header();
?>


<section class="side-title position-relative">


  <div class="left-side">
    <?php if (!my_wp_is_mobile()) { ?>
      <img src="<?= get_template_directory_uri() . '/assets/img/bkg-top.svg' ?>" id="shape-pro-1">
    <?php } ?>
      <div class="title-left-content">
        <h1><?php the_title(); ?></h1>
        <?php the_field('side-title-infos'); ?>
      </div>
  </div>
  <div class="right-side">
      <div class="contact-form-conainer">
        <?php the_content(); ?>
			</div>
  </div>

  <?php if (!my_wp_is_mobile()) { ?>
    <?php
    $image = get_field('side-title_image_bas');
    if ( !empty($image) ): ?>
      <img id="illu1" class="img-bas-contact" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    <?php endif; ?>
  <?php } ?>
</section>


<?php
get_footer();

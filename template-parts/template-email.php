<?php

function admin_mail($DATA){

    $h1 = $DATA->func === "Edit" ? "Modification" : "Ajout" ;

    $mail = '<!DOCTYPE html>';
    $mail .= '<html xmlns="https://www.w3.org/1999/xhtml">';
    $mail .= '<head>';
    $mail .=    '<title><?= esc_html( $title ); ?></title>';
    $mail .=    '<style>td{border: 1px solid #E2E3E3;font-size:16px;font-family:"Calibri";padding:0;white-space:nowrap;padding:10px;}table{transform-origin:left;transform:scale(0.8);}tr{padding:0;margin:0;}</style>';
    $mail .= '</head>';
    $mail .= '<body>';
    $mail .=    "<h1>".$h1." d'établissement</h1>";
    $mail .=    '<h2>'.$DATA->CENTER->NOM.'</h2>';
    $mail .=    '<p>Soumis par '.$DATA->FROM->FULLNAME.'</p>';
    $mail .=    '<p>'.$DATA->FROM->MAIL.'</p>';


    $mail .=    '<table style="font-size:10px;">';
    $mail .=    '<td>'.$DATA->CENTER->NOM.'</td>'; // choix simple
    $mail .=    '<td>'.$DATA->CENTER->ACR.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->ADRESSE.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->CPL.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->CP.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->VILLE.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->DEP.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->COORD.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->TEL1.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->TEL2.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->FAX.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->MAIL.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->WEB.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->HORAIRES.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->STRUCTURE.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->ACTIVITE.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->AIDE.'</td>';
    $mail .=    '<td>';
    $i = 0;
    foreach ($DATA->CENTER->PRODUITS as $value) {
      if($i>0) $mail .= ', ';
      $mail .= $value;
      $i++;
    }
    $mail .=    '</td>';
    $mail .=    '<td>'; // choix multiple
    $i = 0;
    foreach ($DATA->CENTER->EQUIPE as $value) {
      if($i>0) $mail .= ', ';
      $mail .= $value;

      $i++;
    }
    $mail .=    '</td>'; // fin choix multiples
    $mail .=    '<td>'.$DATA->CENTER->PRIX.'</td>';
    $mail .=    '<td>'.$DATA->CENTER->RDV.'</td>';
    $mail .=    '<td>'; // choix multiple
    $i = 0;
    foreach ($DATA->CENTER->PUBLIC as $value) {
      if($i>0) $mail .= ', ';
      $mail .= $value;

      $i++;
    }
    $mail .=    '</td>'; // fin choix multiples
    $mail .=    '<td>'.$DATA->CENTER->INFO.'</td>';
    $mail .=    '<td>'.$DATA->FROM->FULLNAME.'</td>';
    $mail .=    '<td>'.$DATA->FROM->MAIL.'</td>';
    $mail .=    '<td>'.$DATA->FROM->TEL.'</td>';
    $mail .=    '<td>'.$DATA->FROM->POSTE.'</td>';


    $mail .=    '</table>';
    $mail .= '</body>';
    $mail .= '</html>';

    return $mail;
}



function public_mail($DATA){

    $h1 = $DATA->func === "Edit" ? "modification" : "ajout" ;


    $mail = '<!DOCTYPE html>';
    $mail .= '<html xmlns="https://www.w3.org/1999/xhtml">';
    $mail .= '<head>';
    $mail .=    '<title><?= esc_html( $title ); ?></title>';
    $mail .=    '<style></style>';
    $mail .= '</head>';
    $mail .= '<body>';
    $mail .=    "<h1>Votre ".$h1." d'établissement à bien été envoyé !</h1>";
    $mail .=    "<p>Votre ".$h1." pour le centre ".$DATA->CENTER->NOM." sera étudiée dans les meilleurs delais.</p>";
    $mail .=    "<p>Bien à vous</p>";
    $mail .=    "<p>L'équipe Addictoclic | Coreadd</p>";
    $mail .= '</body>';
    $mail .= '</html>';

    return $mail;
}

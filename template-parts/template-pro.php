<?php
/**
* Template Name: Espace Pro
*
* @package WordPress
*/

get_header();
?>


<div <?php post_class('espace-pro'); ?> id="page-<?php the_ID(); ?>">

    <div class="left-side">
      <?php if (!my_wp_is_mobile()) {?>
        <img src="<?= get_template_directory_uri() . '/assets/img/bkg-top.svg' ?>" id="shape-pro-1">
      <?php } ?>

        <div class="pro-content">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>
        <?php if (!my_wp_is_mobile()) {?>
          <img src="<?= get_template_directory_uri() . '/assets/img/bkg-bottom.svg' ?>" id="shape-pro-2">
        <?php } ?>
    </div>


    <div class="right-side">
      <div class="d-flex flex-column">
        <a class="my-btn btn-arrow btn-green btn-arrow-same-height justify-content-start" href="<?= get_home_url(); ?>/formulaire/?add">Ajouter un établissement</a>
        <a class="my-btn btn-arrow btn-orange btn-arrow-same-height justify-content-start" href="<?= get_home_url(); ?>/formulaire/?edit">Modifier la fiche d'un établissement</a>
        <a class="my-btn btn-arrow btn-blue btn-arrow-same-height justify-content-start" href="<?= get_home_url(); ?>/nous-contacter/">Nous contacter</a>
      </div>
    </div>

    <?php if (!wp_is_mobile()) {?>
      <img src="<?= get_template_directory_uri() . '/assets/img/illu.svg' ?>" id="illu1" />
    <?php } ?>
</div>





<?php
get_footer(null, array('no-footer'));

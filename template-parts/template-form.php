<?php
/**
* Template Name: Formulaire
*
* @package WordPress
*/

get_header();
?>


<div <?php post_class('center-form'); ?> id="page-<?php the_ID(); ?>" filters="<?= get_field('csv', 'options')['filtres']['url']; ?>">

    <div class="left-side">
      <?php if (!my_wp_is_mobile()) {?>
        <img src="<?= get_template_directory_uri() . '/assets/img/bkg-top.svg' ?>" id="shape-pro-1">
      <?php } ?>

        <div class="pro-content d-flex flex-column justify-content-center">
          <?php if (my_wp_is_mobile()) {?>
          <div class="d-flex w-100 justify-content-between px-3 align-items-center">
          <?php } ?>

              <h1>
                <?php if(isset($_GET['edit'])){
                    echo "Modifier un établissement";
                }
                else{
                    echo "Ajouter un établissement";
                }
                ?>

              </h1>
              <span id="progress">1/3</span>
            <?php if (my_wp_is_mobile()) {?>
            </div>
            <?php } ?>
            <div class="steps">
                <span class="checked" id="step1"><img src="<?= get_template_directory_uri() . "/assets/img/ic_check_48px.svg" ?>" /> Informations</span>
                <span id="step2"><img src="<?= get_template_directory_uri() . "/assets/img/ic_check_48px.svg" ?>" /> Coordonnées</span>
                <span id="step3"><img src="<?= get_template_directory_uri() . "/assets/img/ic_check_48px.svg" ?>" /> Validation</span>
            </div>
            <?php if (!my_wp_is_mobile()) {?>
              <hr id="steps-hr">
            <?php } ?>
            <?php if (my_wp_is_mobile()) {?>
              <div class="">
            <?php } ?>
              <button id="step-back" target="1">Étape précédente</button>

              <button id="validate" class="my-btn btn-green text-center">Valider ma demande</button>
            <?php if (my_wp_is_mobile()) {?>
            </div>
            <?php } ?>


        </div>

    </div>


    <div class="right-side">

        <div class="form show" id="form1">

            <?php if(isset($_GET['edit'])): ?>
                <div class="position-relative name-list" db-center="<?= get_field('csv', 'options')['centres']['url']; ?>">
                    <p class="name-locked removed"></p>
                    <input id="name" name="NOM" hidden>
                    <?php if(!isset($_GET['ID'])): ?>
                        <button class="my-btn btn-green mt-3 text-center" style="order:2" id="valid-editing">Je souhaite modifier cet établissement</button>
                    <?php endif; ?>
                </div>
            <?php else: ?>
              <div class="position-relative">
                <img class="icon-edit-name" src="<?= get_template_directory_uri() . "/assets/img/pencil.svg" ?>" />
                <input id="name" name="NOM" placeholder="Nom de l'établissement...">
              </div>
            <?php endif; ?>



            <div class="forms step1" <?php if(isset($_GET['edit'])) echo 'style="display:none"'; ?>>

                <button id="end-step1" class="my-btn btn-arrow btn-green mr-auto mt-5">Étape suivante</button>

            </div>

        </div>


        <div class="form" id="form2">
            <?php if(isset($_GET['edit'])): ?>
                <div class="position-relative">
                    <p class="name-locked-2" style="color: #000"></p>
                </div>
            <?php else: ?>
              <div class="d-flex align-items-center">
                <img style="width: 28px" src="<?= get_template_directory_uri() . "/assets/img/ic_lock_48px.svg" ?>" />
                <p id="name-lock" class="mb-0 ml-3"></p>
            </div>
            <?php endif; ?>

            <hr id="form2-hr" class="mt-4">

            <div class="forms step2">

                <button id="end-step2" class="my-btn btn-arrow btn-green mr-auto mt-5">Étape suivante</button>

            </div>

        </div>


        <div class="form" id="valid-form">

            <h2 id="v-name"></h2>

            <div class="confirm-1">
                <h3>Informations</h3>
                <p id="v-type">Type : <span></span></p>
                <p id="v-act">Activité : <span></span></p>
                <p id="v-aide">Aide : <span></span></p>
                <p id="v-prod">Produit(s) : <span></span></p>
                <p id="v-ekip">Équipe : <span></span></p>
                <p id="v-info">Infos complémentaires : <span></span></p>
                <p id="v-pub">Public : <span></span></p>
            </div>

            <div class="confirm-2">
                <h3>Coordonnées</h3>
                <p id="v-addr"><span></span></p>
                <p id="v-comp"><span></span></p>
                <p id="v-gps"><span></span></p>
                <p id="v-tel"><img src="<?= get_template_directory_uri() . "/assets/img/phone-2.svg" ?>"> <span></span></p>
                <p id="v-tel2"><img src="<?= get_template_directory_uri() . "/assets/img/phone-2.svg" ?>"> <span></span></p>
                <p id="v-fax">Fax : <span></span></p>
                <p id="v-mail"><img src="<?= get_template_directory_uri() . "/assets/img/ic_email_24px.svg" ?>"> <span></span></p>
                <p id="v-web"><img src="<?= get_template_directory_uri() . "/assets/img/link-72.svg" ?>"> <span></span></p>
                <p id="v-hor"><span></span></p>

            </div>

        </div>

    </div>

    <div class="modal modal-validate faade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Vérification de votre identité</h3>
                    <button type="button" class="close exit" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 mb-4 mb-md-0">
                            <h4 class="mb-4  mt-5 mt-md-0 text-center text-md-left" style="color:black">Informations personnelles</h4>
                            <input type="text" name="from_name" placeholder="Prénom Nom*" id="from-name" />
                            <input type="text" name="from_poste" placeholder="Poste / Fonction*" id="from-poste" />
                            <input type="text" name="from_tel" placeholder="Téléphone*" id="from-tel" />
                            <input type="text" name="from_mail" placeholder="Mail*" id="from-mail" />

                        </div>
                        <div class="col-md-6">
                            <h4 style="margin-bottom:1.5em;">Afin que notre équipe traite votre demande, merci de renseigner les informations vous concernant.</h4>
                            <p>Vos coordonnées nous permettrons de vous contacter si votre fiche établissement est incomplète ou contient des erreurs.</p>
                            <input type="checkbox" id="confirm-send" name="rdy-to-send">
                            <p style="color:rgb(var(--orange));display:inline;">* Je certifie sur l’honneur que les informations fournies correspondent à la réalité et d'etre en droit de les soumettre à COREADD.</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="my-btn btn-orange mb-0 exit">Fermer</button>
                    <a type="button" id="send" class="my-btn btn-green mb-0" data-dismiss="modal">Envoyer ma demande</a>
                </div>
            </div>
        </div>
    </div>



    <div class="modal modal-error fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Champs manquant !</h3>
                    <button type="button" class="close exit" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="step-error">Message d'erreur</p>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="toDelete admin-url" admin-url="<?= admin_url('admin-ajax.php'); ?>"></div>



<?php
get_footer(null, array('no-footer'));

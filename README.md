![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr/socreativ/coreadd)
![](https://img.shields.io/badge/PHP-7.4.29-informational)

# Addictoclic

Addictoclic est une carte *interactive* affichant une liste de centres d'addictologie de Nouvelle Aquitaine. Les 2 types d'utilisateurs sur le site sont : 

 - Personnes cherchant un centre à travers la carte
 - Administration de centre voulant afficher son centre sur la carte et mettre à jour les informations de celui-ci

## CSV

La liste des centres est maintenu par l'association Coreadd via un excel. Il est insérer en backoffice à l'aide d'un champs ACF. Puis il est chargé et converti en objet javascript. C'est pour cela qu'il doit suivre une structure bien précise tel que :

|     Clé       | Description 
|---------------|-------------
| NOM           | Nom du centre 
| ACR           | Acronyme du centre 
| ADRESSE       | Adresse du centre 
| CPL           | Code postal du centre
| VILLE         | Ville du centre
| DEP           | Département du centre
| COORD         | Coordonnées GPS (Seront généré automatiquement si vide)
| TEL1          | Numéro de téléphone N°1
| TEL2          | Numéro de téléphone N°2
| FAX           | Numéro de FAX
| MAIL          | Adresse email	
| WEB           | Site web
| HORAIRES      | Horaires d'ouverture
| STRUCTURE     | Type de structure du centre
| ACTIVITE      | Activité (Consultation, permanence, etc..)
| AIDE          | Type de d'aide (Soin etc...)
| PRODUITS      | Type de produits / addiction pris en charge (Alcool,sexe, drogue, etc...)
| EQUIPE        | Type d'équipe du centre (Médecin, infirmier etc..)
| PRIX          | Prix des consultations / soins 
| RDV           | Consultation sur RDV ou non
| PLUBLIC       | Public visée (Tout public, ado, femme enceinte, etc..)
| INFO          | Information complémentaire à propos du centre
| CONTACT_NOM   | Nom du contact gérant le centre
| CONTACT_TEL   | Numéro de téléphone du contact gérant le centre
| CONTACT_POST  | Poste / fonction du contact gérant le centre



**Notes:** 
Le fichier CSV est chargé en javascript avec l'ancienne méthode [XMLHttpRequest](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest) et qui pourrait être optimisé en la remplaçant par la méthode [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch).


## Map

La map utilise l'API Google Map, sur laquelle nous ajoutons les marqueurs correspondant à chaque centre. 

Pour chaque centre une marqueur est crée et ajouter à la map.
Lors d'une recherche les marqueurs sont supprimé et recrée à partir d'un tableau de centre filtré puis ré-ajouter à la map.

Une fonction de Geocodding est également présente dans le cas où un centre n'aurais pas de coordonnées dans le tableau excel. 

C'est la classe Map qui gère également la liste des centres à gauche de la map. De la même manière que pour les marqueurs ont crée à partir du tableau des centres une carte basé sur un template.

## Formulaire

Une formulaire de contact destinée au gérant de centre à été crée. Il permet à n'importe de qui de proposer un ajout ou une modification de centre. <br>
Il est composé de plusieurs étapes, avec chacune ces vérifications. Le formulaire d'édition est simplement le formulaire d'ajout pré-remplie grâce au données récupérer depuis le CSV avec l'ID en paramètre. 

## Filtre

Le filtres est juste un eventListener sur le bouton submit, il récupère tout les valeurs de tout les input présent dans l'éléments parents, puis à l'aide d'une méthode propre à la map (newSearch) cela permet de trier dans la liste des marqueur / centres et d'afficher ceux qui corresponde à la recherche. 

Le filtre ne supprime pas les éléments qui ne correspondent pas mais les cache seulement. Donc lors d'une nouvelle recherche les centres ne sont pas recrée mais simplement ré-afficher.

## Input

Pour les champs de recherche du filtre où le formulaire une structure d'input à du être recrée, notamment pour les champs select multiple. Une classe **Filter** a été crée pour permettre de crée des champs select simple ou multiple facilement juste en donnant différents paramètres (isMultiple, placeholder, label etc..)


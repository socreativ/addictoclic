<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */

 $css = isset($args[0]) && $args[0] == 'no-scroll' ? 'no-scroll' : '';

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php echo get_field('head_scripts', 'options') ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<?php the_field( "google_font","option" ); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class($css); ?>>
<?php wp_body_open(); ?>

<style>
    :root{--font: <?= get_field('fontname', 'options') ?>}
</style>

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'theme-by-socreativ' ); ?></a>

  <?php if (is_page_template('template-parts/template-map.php')) {?>
    <span id="theme_dir" style="display: none;"><?= get_template_directory_uri(); ?></span>
    <div class="loader">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <header class="map-header">
      <div class="main-header container-fluid">
  <?php } else {?>

    <header class="">
      <div class="main-header container">

  <?php }?>
          <?php
          $image =  get_field('logo','options')['dark'];
          if ( !empty($image) ): ?>
            <a href="<?= get_home_url(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
          <?php endif; ?>

            <nav>
                <?php if (!my_wp_is_mobile()) {?>
                  <?php
                      wp_nav_menu( array(
                          'theme_location' => 'main-menu',
                          'container_class' => '',
                          'container' => 'ul',
                          'menu_id' => '',
                          'menu_class' => 'list-unstyled main-menu-container', ) );
                  ?>

                  <span class="has-orange-color mx-3">  |  </span>
                <?php } ?>
                <div class="burger menu-toggle d-flex ml-3">
                  <span>MENU</span>
                  <div class="burger-cross d-block position-relative ml-3">
      							<span class="hamb-line anim-300"></span>
      							<span class="hamb-line anim-300"></span>
      							<span class="hamb-line anim-300 m-0"></span>
      							<span class="crose-line anim-300"></span>
      							<span class="crose-line crose-line2 anim-300"></span>
      						</div>
                </div>
                <div class="burger-nav">
                  <button type="button" class="close exit menu-toggle" aria-label="Close">
                        <span aria-hidden="true">×</span>
                  </button>
                  <div class="overflow-y-nav">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'burger-menu',
                            'container_class' => '',
                            'container' => 'ul',
                            'menu_id' => '',
                            'menu_class' => 'list-unstyled burger-menu-container my-auto', ) );
                    ?>
                    <div class="infos-contact-nav p-5">
                      <a href="<?= get_home_url(); ?>"><img src="<?= get_template_directory_uri() . '/assets/img/LOGO-coreadd.svg' ?>" /></a>
                      <p><?php the_field('desc','option') ?></p>
                      <h2><?php the_field('baseline','option') ?></h2>
                    </div>
                  </div>
                </div>
                <div class="mobile-nav-mask"></div>
            </nav>
        </div>
        <?php if (is_page_template('template-parts/template-map.php')) {?>
        <?php if (my_wp_is_mobile()) {?>
          <div class="mobile-form my-btn btn-green mx-auto mb-0 my-O py-2 text-white">
            Critères de recherche
          </div>
        <?php } ?>
        <div class="filtre-container container">
            <div id="filtre-input">
                <button class="search-submit">Valider la recherche</button>
            </div>
        </div>
        <?php }?>
	</header>
  <?php if (my_wp_is_mobile() && is_page_template('template-parts/template-map.php')) {?>
    <div class="mobile-form-mask"></div>
  <?php } ?>

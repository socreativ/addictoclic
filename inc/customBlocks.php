<?php

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Spacer'),
            'description'       => __('margin block'),
            'render_template'   => 'template-parts/blocks/spacing/spacing.php',
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
        ));
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/slider/slider.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array(  'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
        ));
        acf_register_block_type(array(
            'name'              => 'glossaire',
            'title'             => __('Glossaire'),
            'description'       => __('Glossaire'),
            'render_template'   => 'template-parts/blocks/glossaire/glossaire.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array(  'glossaire', 'recherche', 'mots'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/glossaire/glossaire.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/glossaire/glossaire.css',
        ));
        acf_register_block_type(array(
            'name'              => 'contact',
            'title'             => __('Contact'),
            'description'       => __('Contact'),
            'render_template'   => 'template-parts/blocks/contact/contact.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array(  'contact'),
            'supports'          => array( 'anchor' => true),
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/contact/contact.css',
        ));
        add_action('acf/init', 'register_acf_block_types');
    }


?>

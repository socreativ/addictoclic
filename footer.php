<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme-by-socreativ
 */
?>
	<?php if(get_field('isModal')): ?>
		<?php if (get_field('modal_type')): $content = get_field('notif_content'); ?>
			<?php if(!my_wp_is_mobile()): ?>
			<div class="notification anim-300 d-none d-md-block closed" data-delay="<?php echo get_field('delay'); ?>">
				<button type="button" class="close" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<div class="notification-logo-left">
					<img src="<?php echo $content['logo']['sizes']['thumbnail'] ?>" alt="<?php echo $content['logo']['alt'] ?>" height="<?php echo $content['logo']['height'] ?>" width="<?php echo $content['logo']['width'] ?>">
				</div>
				<div class="d-flex">
					<div>
						<p><?php echo $content['texte'] ?></p>
					</div>
				</div>
				<div class="d-flex justify-center">
						<a class="custom-btn" target="<?php echo $content['bouton']['target'] ?>" href="<?php echo $content['bouton']['url'] ?>"><?php echo $content['bouton']['title'] ?></a>
				</div>
			</div>
			<?php endif; ?>
		<?php else: $content = get_field('modal_content'); ?>
			<div class="modal custom-modal closed" tabindex="-1" role="dialog" data-delay="<?php echo get_field('delay'); ?>">
				<div class="modal-dialog <?php if($content['visuel'] && !my_wp_is_mobile()) echo 'modal-big' ?> modal-dialog-centered" role="document">
					<?php if($content['visuel'] && !my_wp_is_mobile()): ?>
						<img class="modal-visuel" src="<?php echo $content['visuel']['sizes']['medium'] ?>" alt="<?php echo $content['visuel']['alt'] ?>" height="<?php echo $content['visuel']['height'] ?>" width="<?php echo $content['visuel']['width'] ?>">
 					<?php endif; ?>
					<div class="modal-content p-3">
					<div class="modal-header">
						<h2 class="modal-title"><?php echo $content['titre']; ?></h2>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p><?php echo $content['texte']; ?></p>
					</div>
					<div class="modal-footer">
						<a class="custom-btn" target="<?php echo $content['bouton']['target'] ?>" href="<?php echo $content['bouton']['url'] ?>"><?php echo $content['bouton']['title'] ?></a>
					</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<!-- BEGIN FOOTER -->
	<?php if(isset($args[0]) && $args[0] != 'no-footer'): ?>
	<footer id="colophon" class="site-footer">

		<div class="footer-container">
			<div class="footer-banner">
				<div class="logo-footer container py-4">
					<div class="row align-items-center">
						<div class="col-12 col-lg-3 text-center text-lg-left justify-content-center justify-content-lg-end  my-3 my-lg-0">
							<?php if(get_field('name', 'options')): ?>
								<h2 class="w-100 pb-0 mb-0 uppercase"><?php echo get_field('name', 'options'); ?></h2>
							<?php endif; ?>
							<?php if(get_field('desc', 'options')): ?>
								<p class="pb-0 mb-0 uppercase"><?php echo get_field('desc', 'options'); ?></p>
							<?php endif; ?>
							<?php if(get_field('baseline', 'options')): ?>
								<p class="mb-0 mt-4"><?php echo get_field('baseline', 'options'); ?></p>
							<?php endif; ?>
						</div>

						<div class="col-12 col-lg-4 ml-lg-auto text-center text-lg-left justify-content-center justify-content-lg-end  align-items-start my-3 my-lg-0">
							<?php
									wp_nav_menu(
										array(
											'theme_location' => 'footer',
											'container_class' => '',
											'container' => 'ul',
											'menu_id' => '',
											'menu_class' => 'lh-15 ml-0 pl-0 list-unstyled footer-nav',
										)
									);
							?>
						</div>
						<?php if (!my_wp_is_mobile()) { ?>
							<div class="col-12 col-lg-4 text-center text-lg-right justify-content-center justify-content-lg-end  my-3 my-lg-0">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/footer/logo-footer.svg" alt="logo image footer addictoclic">
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="sub-footer">
			<div class="container">
				<div class="col-12 sub-footer-legacy text-center fs-12 py-2">
					<?php dynamic_sidebar( 'sub-footer-widget' ); ?>
				</div>
		</div>
	</footer>
	<?php endif; ?>
<?php wp_footer(); ?>
</body>
<?php echo get_field('footer_scripts', 'options') ?>
</html>
